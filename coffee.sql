--
-- File generated with SQLiteStudio v3.2.1 on พ. พ.ย. 2 01:15:17 2022
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: customer
DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
    cus_id       INTEGER       PRIMARY KEY ASC AUTOINCREMENT
                               NOT NULL,
    cus_name     TEXT (30)     NOT NULL,
    cus_surname  TEXT (30)     NOT NULL,
    cus_tel      CHAR (10)     NOT NULL
                               UNIQUE,
    cus_point    INTEGER       NOT NULL,
    cus_nop      INTEGER       NOT NULL,
    cus_discount DOUBLE (4, 2) NOT NULL
);

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         1,
                         'วินัย',
                         'งดงาม',
                         '062-222-5566',
                         100,
                         20,
                         5.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         2,
                         'ธนพล',
                         'ศรีรัก',
                         '061-254-9865',
                         80,
                         16,
                         3.2
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         3,
                         'อรอนงค์',
                         'งดงาม',
                         '080-885-6845',
                         60,
                         12,
                         2.4
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         4,
                         'ดอกรัก',
                         'สุวรรณ',
                         '096-598-7826',
                         120,
                         24,
                         4.8
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         5,
                         'โชคชัย',
                         'นอบน้อม',
                         '092-964-9631',
                         110,
                         20,
                         4.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         6,
                         'พรพิมล',
                         'ทองสวย',
                         '086-662-9987',
                         65,
                         12,
                         2.4
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         7,
                         'เอกนงค์',
                         'เมฆา',
                         '065-987-1235',
                         75,
                         13,
                         2.6
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         8,
                         'คงนิล',
                         'วินัย',
                         '080-456-7891',
                         200,
                         39,
                         7.8
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         9,
                         'ญาณิกา',
                         'คงภักดี',
                         '091-669-8520',
                         140,
                         28,
                         5.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         10,
                         'กรวิชญ์',
                         'ศศิระบวรพัฒน์',
                         '056-339-1147',
                         11,
                         4,
                         0.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         11,
                         'เบญญาภา',
                         'บุญวิสูตร',
                         '062-513-3422',
                         122,
                         13,
                         0.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         12,
                         'เรือนแก้ว',
                         'กองขุนทด',
                         '081-223-8788',
                         140,
                         14,
                         5.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         13,
                         'ศศินา',
                         'มู้เม่า',
                         '053-225-9183',
                         110,
                         20,
                         10.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         14,
                         'ธัญญ่า',
                         'อัครวิมลกุล',
                         '095-229-9922',
                         120,
                         40,
                         2.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         15,
                         'ภูษณิศา',
                         'จารุเนตร',
                         '091-845-9239',
                         20,
                         39,
                         5.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         16,
                         'จิตติมาศ',
                         'วิรรณวิจิตร',
                         '078-334-8938',
                         35,
                         20,
                         10.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         17,
                         'ซูไรดา',
                         'บินเยาะ',
                         '094-345-8888',
                         16,
                         15,
                         0.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         18,
                         'นนทพัฒธ์',
                         'วิเศษวงษา',
                         '098-112-3456',
                         60,
                         12,
                         0.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         19,
                         'โจศักดิ์',
                         'วรพันธ์',
                         '094-883-9588',
                         30,
                         11,
                         10.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         20,
                         'เอกชัย',
                         'ศรีวิชัย',
                         '065-119-9911',
                         110,
                         20,
                         15.0
                     );

INSERT INTO customer (
                         cus_id,
                         cus_name,
                         cus_surname,
                         cus_tel,
                         cus_point,
                         cus_nop,
                         cus_discount
                     )
                     VALUES (
                         24,
                         'สุวรรณลักษณ์',
                         'หิรัญลักษณา',
                         '0913391719',
                         13,
                         28,
                         0.0
                     );


-- Table: emp_report
DROP TABLE IF EXISTS emp_report;

CREATE TABLE emp_report (
    empReport_id   INTEGER PRIMARY KEY ASC AUTOINCREMENT
                           NOT NULL,
    emp_id         INTEGER REFERENCES employee (emp_id) ON DELETE CASCADE
                                                        ON UPDATE CASCADE
                           NOT NULL,
    tc_report_id   INTEGER REFERENCES timeclock_report (tc_report_id) ON DELETE CASCADE
                                                                      ON UPDATE CASCADE
                           NOT NULL,
    total_salary   DOUBLE  NOT NULL,
    total_hours    INTEGER NOT NULL,
    empReport_date DATE    NOT NULL
);

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           1,
                           1,
                           1,
                           '2,300',
                           51,
                           '2022/1/05'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           2,
                           2,
                           2,
                           '2,150',
                           47,
                           '2022/1/12'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           3,
                           3,
                           3,
                           '2,670',
                           59,
                           '2022/1/13'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           4,
                           4,
                           4,
                           '2,230',
                           49,
                           '2022/1/17'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           5,
                           5,
                           5,
                           '2,050',
                           45,
                           '2022/1/22'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           6,
                           6,
                           6,
                           '3,000',
                           66,
                           '2022/1/30'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           7,
                           7,
                           7,
                           '2,700',
                           60,
                           '2022/2/03'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           8,
                           8,
                           8,
                           '2,450',
                           54,
                           '2022/2/16'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           9,
                           9,
                           9,
                           '2,300',
                           51,
                           '2022/2/12'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           10,
                           10,
                           10,
                           '2,940',
                           65,
                           '2022/2/25'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           11,
                           11,
                           11,
                           '2,250',
                           50,
                           '2022/3/02'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           12,
                           12,
                           12,
                           '2,090',
                           46,
                           '2022/3/05'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           13,
                           13,
                           13,
                           '2,100',
                           46,
                           '2022/3/13'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           14,
                           14,
                           14,
                           '2,400',
                           53,
                           '2022/3/17'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           15,
                           15,
                           15,
                           '2,150',
                           47,
                           '2022/3/22'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           16,
                           16,
                           16,
                           '2,850',
                           63,
                           '2022/3/24'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           17,
                           17,
                           17,
                           '2,650',
                           58,
                           '2022/3/30'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           18,
                           18,
                           18,
                           '2,600',
                           57,
                           '2022/4/02'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           19,
                           19,
                           19,
                           '2,750',
                           61,
                           '2022/4/04'
                       );

INSERT INTO emp_report (
                           empReport_id,
                           emp_id,
                           tc_report_id,
                           total_salary,
                           total_hours,
                           empReport_date
                       )
                       VALUES (
                           20,
                           20,
                           20,
                           '2,560',
                           56,
                           '2022/4/06'
                       );


-- Table: employee
DROP TABLE IF EXISTS employee;

CREATE TABLE employee (
    emp_id       INTEGER    PRIMARY KEY ASC AUTOINCREMENT,
    emp_name     TEXT (50)  NOT NULL,
    emp_surname  TEXT (50)  NOT NULL,
    emp_tel      CHAR (10)  UNIQUE
                            NOT NULL,
    emp_user     TEXT (50)  UNIQUE
                            NOT NULL,
    emp_password TEXT (10)  UNIQUE
                            NOT NULL,
    emp_position CHAR (1)   NOT NULL,
    emp_gender   CHAR (1)   NOT NULL,
    emp_dob      TEXT       NOT NULL,
    emp_nat      TEXT (30)  NOT NULL,
    emp_religion TEXT (20)  NOT NULL,
    emp_status   TEXT (20)  NOT NULL,
    emp_address  TEXT (200) NOT NULL
);

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         1,
                         'ประหยัด',
                         'จันทร์อังคาร',
                         '061-000-1111',
                         'prayat@gmail.com',
                         'prayat1234',
                         '2',
                         'ชาย',
                         '2001/05/09',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '16/1 หมู่ 4 ถนนบำรุงราษฎร์ ตำบลพิบูลสงคราม อำเภอเมือง กรุงเทพมหานคร 10400 '
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         2,
                         'ปลาวิด',
                         'วงสวรรค์',
                         '061-000-1112',
                         'prawit@gmail.com',
                         'prawit1234',
                         '2',
                         'ชาย',
                         '2000/08/26',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '7/22 หมู่ 5 ซอยท่าเอียด (เจ้าฟ้า 50) ถนนเจ้าฟ้าตะวันตก ตำบอลฉลอง อำเภอเมือง จังหวัดภูเก็ต 83000'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         3,
                         'ปราณี',
                         'วิไล',
                         '061-000-1113',
                         'pranee@gmail.com',
                         'pranee1234',
                         '2',
                         'หญิง',
                         '2000/12/11',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '228/24-25 ถนนลาดพร้าว จอมพร, จตุจักร จังหวัด กรุงเทพมหานคร 10900'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         4,
                         'ปรากฎ',
                         'สาครินทร์',
                         '061-000-1114',
                         'pragod@gmail.com',
                         'pragod1234',
                         '2',
                         'ชาย',
                         '2000/08/24',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         'บ้านเลขที่ 46 หมู่ 9 หมู่บ้านนกน้อย ซอยรัชดาภิเษก 66 เขตบางซื่อ แขวงบางซื่อกทม. 10800'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         5,
                         'อมรเทพ',
                         'มรวิเวช',
                         '061-000-1115',
                         'amornthep@gmail.com',
                         'amornthep1234',
                         '2',
                         'ชาย',
                         '2001/03/01',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         'บ้านเลขที่ 39 หมู่ที่ 3 ตำบลหนองควาย อำเภอหางดง จังหวัดเชียงใหม่'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         6,
                         'อภิรักษ์',
                         'อนุณิชกุล',
                         '061-000-1116',
                         'apirak@gmail.com',
                         'apirak1234',
                         '2',
                         'ชาย',
                         '2001/03/9',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '20/21 หมู่ 1 ถนนเจริญกรุง ตำบลเเควใหญ่ อำเภอเมือง กรุงเทพมหานคร 10403'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         7,
                         'อดิศักดิ์',
                         'บุญวัชร',
                         '061-000-1117',
                         'adisak@gmail.com',
                         'adisak1234',
                         '2',
                         'ชาย',
                         '2002/07/21',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '451 ถนน พระรามที่ 1 แขวงปทุมวัน เขตปทุมวัน กรุงเทพมหานคร 10330'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         8,
                         'อัญชนา',
                         'วิทยพันธ์ุ',
                         '061-000-1118',
                         'auncahna@gmail.com',
                         'auncahna1234',
                         '1',
                         'หญิง',
                         '1999/12/20',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '30/39-50 หมู่ 2 ถนนงามวงศ์วาน ตำบลบางเขน อำเภอเมือง จังหวัดนนทบุรี 11000'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         9,
                         'โสภา',
                         'วิภา',
                         '061-000-1119',
                         'sopa@gmail.com',
                         'sopa1234',
                         '2',
                         'หญิง',
                         '1999/4/26',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '333/99 หมู่ที่ 9 ถนนพัทยาสายสอง เมืองพัทยา อำเภอบางละมุง จังหวัดชลบุรี 20150'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         10,
                         'น้ำหนึ่ง',
                         'ยินดี',
                         '061-000-1120',
                         'numnoung@gmail.com',
                         'numnoung1234',
                         '2',
                         'หญิง',
                         '2002/05/04',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '99/9 หมู่ที่ 13 ถ.พหลโยธิน อำเภอเมืองเชียงราย จังหวัดเชียงราย 57000'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         11,
                         'กษมา',
                         'จำนงค์ลาภ',
                         '061-000-1121',
                         'kasama@gmail.com',
                         'kasama1234',
                         '2',
                         'ชาย',
                         '1998/04/23',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '256/344 หมู่ 4 ถนนรามคำแหง ตำบลคลองกุ่ม อำเภอบึงกุ่ม กรุงเทพมหานคร 12400'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         12,
                         'พิชิต',
                         'วะละยุน',
                         '061-000-1122',
                         'pichit@gmail.com',
                         'pichit1234',
                         '1',
                         'ชาย',
                         '2000/10/30',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '42 หมู่ 1 บ้านสังแก ตำบลสะเดา อำเภอบัวเชด จังหวัดสุรินทร์ 32230'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         13,
                         'วรคณิต',
                         'วีระร้อย',
                         '061-000-1123',
                         'worakanit@gmail.com',
                         'worakanit1234',
                         '2',
                         'ชาย',
                         '1997/06/1',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '17/4 หมู่ 5 ถนนบำรุงราษฎร์ ตำบลพิบูลสงคราม อำเภอเมือง เชียงราย 51000'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         14,
                         'เปรมปรีดา',
                         'สลับสี',
                         '061-000-1124',
                         'prempreda@gmail.com',
                         'prempreda1234',
                         '2',
                         'ชาย',
                         '1997/11/12',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '35/88 หมู่บ้านริชชี่ ซอยมหาทรัพย์ ถนนเศรษฐี แขวงบางเล็ก เขตจอมทัพ กรุงเทพมหานคร 10880'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         15,
                         'องอาจ',
                         'ไปสุรินทร์',
                         '061-000-1125',
                         'oangarj@gmail.com',
                         'oangarj1234',
                         '2',
                         'ชาย',
                         '2001/05/25',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '888/66 หมู่ 2 ถนนรามคำแหง ตำบลคลองกุ่ม อำเภอบึงกุ่ม กรุงเทพมหานคร 12400'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         16,
                         'อภัสรา',
                         'วรรณวิจิตร',
                         '061-000-1126',
                         'passara@gmail.com',
                         'passara1234',
                         '2',
                         'หญิง',
                         '1998/10/4',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '66 หมู่ 9 บ้านผักหวาน ตำบลสะเดา อำเภอบัวเชด จังหวัดสุรินทร์ 32230'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         17,
                         'ซาเนีย',
                         'หวังใจ',
                         '061-000-1127',
                         'sania@gmail.com',
                         'sania1234',
                         '2',
                         'หญิง',
                         '1999/05/7',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '67 หมู่ 1 ต.ตาคลี อ. ตาคลี จ.นครสวรรค์ 63140'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         18,
                         'ปริยากร',
                         'เหลืองกระจ่าง',
                         '061-000-1128',
                         'pariyagorn@gmail.com',
                         'pariyagorn1234',
                         '2',
                         'หญิง',
                         '2001/09/18',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '186/ม.4 ต.เหนือคลอง อ.เหนือคลอง จ.กระบี่ 81130'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         19,
                         'ศศิธร',
                         'แมงเม่า',
                         '061-000-1129',
                         'sasithorn@gmail.com',
                         'sasithorn1234',
                         '2',
                         'หญิง',
                         '1999/02/24',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '80/2 ถ.แสงชูโต ต.ท่ามะขาม อ.เมืองกาญจนบุรี จ.กาญจนบุรี'
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_surname,
                         emp_tel,
                         emp_user,
                         emp_password,
                         emp_position,
                         emp_gender,
                         emp_dob,
                         emp_nat,
                         emp_religion,
                         emp_status,
                         emp_address
                     )
                     VALUES (
                         20,
                         'มกรา',
                         'ธันวา',
                         '061-000-1130',
                         'makara@gmail.com',
                         'makara1234',
                         '2',
                         'ชาย',
                         '2001/09/29',
                         'ไทย',
                         'พุทธ',
                         'โสด',
                         '12 ต. จันทนิมิต อ.เมืองจันทบุรี จ.จันทบุรี'
                     );


-- Table: material
DROP TABLE IF EXISTS material;

CREATE TABLE material (
    mtr_id     INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                         NOT NULL,
    mtr_name   TEXT (50) UNIQUE
                         NOT NULL,
    mtr_min    INTEGER   NOT NULL,
    mrt_onhand INTEGER   NOT NULL,
    mtr_unit   TEXT (15) NOT NULL
);

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         1,
                         'เมล็ดกาแฟ',
                         20,
                         25,
                         'ถุง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         2,
                         'ผงชาเขียว',
                         20,
                         15,
                         'ซอง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         3,
                         'ผงชาไทย',
                         10,
                         12,
                         'ซอง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         4,
                         'ผงโกโก้',
                         10,
                         25,
                         'ซอง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         5,
                         'ผงช็อกโกแลต',
                         10,
                         18,
                         'ซอง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         6,
                         'ผงเนสที',
                         10,
                         10,
                         'ซอง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         7,
                         'ผงแอปเปิ้ล',
                         10,
                         26,
                         'ซอง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         8,
                         'นมหมี',
                         10,
                         6,
                         'กระป๋อง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         9,
                         'นมสดเมจิ',
                         5,
                         2,
                         'แกลลอน'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         10,
                         'โอรีโอ้',
                         10,
                         5,
                         'ซอง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         11,
                         'น้ำตาลทราย',
                         10,
                         2,
                         'กิโลกรัม'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         12,
                         'ครีมเทียม',
                         10,
                         3,
                         'ซอง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         13,
                         'นมข้นจืด',
                         10,
                         2,
                         'กระป๋อง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         14,
                         'นมข้นหวาน',
                         10,
                         2,
                         'กระป๋อง'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         15,
                         'ไซรับ',
                         3,
                         7,
                         'ขวด'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         16,
                         'น้ำเชื่อม',
                         5,
                         2,
                         'ลิตร'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         17,
                         'วิปปิ้งครีม',
                         5,
                         7,
                         'ลิตร'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         18,
                         'น้ำส้ม',
                         5,
                         2,
                         'ขวด'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         19,
                         'น้ำแอปเปิ้ล',
                         5,
                         3,
                         'ขวด'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         20,
                         'สตรอว์เบอร์รี',
                         5,
                         4,
                         'ขวด'
                     );

INSERT INTO material (
                         mtr_id,
                         mtr_name,
                         mtr_min,
                         mrt_onhand,
                         mtr_unit
                     )
                     VALUES (
                         21,
                         'คาราเมล',
                         10,
                         0,
                         'กิโลกรัม'
                     );


-- Table: order
DROP TABLE IF EXISTS [order];

CREATE TABLE [order] (
    order_id   INTEGER       PRIMARY KEY AUTOINCREMENT
                             NOT NULL,
    store_id   INTEGER       NOT NULL
                             REFERENCES store (store_id) ON DELETE CASCADE
                                                         ON UPDATE CASCADE,
    cus_id     INTEGER       NOT NULL
                             REFERENCES customer (cus_id) ON DELETE CASCADE
                                                          ON UPDATE CASCADE,
    emp_id     INTEGER       NOT NULL
                             REFERENCES employee (emp_id) ON DELETE CASCADE
                                                          ON UPDATE CASCADE,
    total      DOUBLE (4, 2) NOT NULL,
    discount   NUMERIC       NOT NULL,
    cash       DOUBLE        NOT NULL,
    change     NUMERIC       NOT NULL,
    queue_name INTEGER       NOT NULL
);

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        1,
                        1,
                        1,
                        1,
                        55.0,
                        5,
                        60.0,
                        10,
                        1
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        2,
                        2,
                        2,
                        2,
                        110.0,
                        10,
                        100.0,
                        '-',
                        2
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        3,
                        3,
                        3,
                        3,
                        65.0,
                        '-',
                        100.0,
                        35,
                        3
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        4,
                        4,
                        4,
                        4,
                        60.0,
                        5,
                        60.0,
                        5,
                        4
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        5,
                        5,
                        5,
                        5,
                        195.0,
                        15,
                        200.0,
                        20,
                        5
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        6,
                        6,
                        6,
                        6,
                        65.0,
                        '-',
                        65.0,
                        '-',
                        6
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        7,
                        7,
                        7,
                        7,
                        80.0,
                        10,
                        80.0,
                        10,
                        7
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        8,
                        8,
                        8,
                        8,
                        50.0,
                        '-',
                        100.0,
                        50,
                        8
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        9,
                        9,
                        9,
                        9,
                        90.0,
                        10,
                        80.0,
                        '-',
                        9
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        10,
                        10,
                        10,
                        10,
                        35.0,
                        '-',
                        50.0,
                        15,
                        10
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        11,
                        11,
                        11,
                        11,
                        110.0,
                        '-',
                        110.0,
                        '-',
                        11
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        12,
                        12,
                        12,
                        12,
                        55.0,
                        5,
                        60.0,
                        10,
                        12
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        13,
                        13,
                        13,
                        13,
                        60.0,
                        '-',
                        60.0,
                        '-',
                        13
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        14,
                        14,
                        14,
                        14,
                        90.0,
                        10,
                        80.0,
                        '-',
                        14
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        15,
                        15,
                        15,
                        15,
                        80.0,
                        '-',
                        100.0,
                        20,
                        15
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        16,
                        16,
                        16,
                        16,
                        200.0,
                        5,
                        200.0,
                        5,
                        16
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        17,
                        17,
                        17,
                        17,
                        195.0,
                        '-',
                        200.0,
                        5,
                        17
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        18,
                        18,
                        18,
                        18,
                        65.0,
                        '-',
                        80.0,
                        15,
                        18
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        19,
                        19,
                        19,
                        19,
                        85.0,
                        10,
                        100.0,
                        25,
                        19
                    );

INSERT INTO [order] (
                        order_id,
                        store_id,
                        cus_id,
                        emp_id,
                        total,
                        discount,
                        cash,
                        change,
                        queue_name
                    )
                    VALUES (
                        20,
                        20,
                        20,
                        20,
                        100.0,
                        '-',
                        100.0,
                        '-',
                        20
                    );


-- Table: order_material
DROP TABLE IF EXISTS order_material;

CREATE TABLE order_material (
    order_mtr_id INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                           NOT NULL,
    order_id     INTEGER   REFERENCES order_report (order_id) ON DELETE CASCADE
                                                              ON UPDATE CASCADE
                           NOT NULL,
    mtr_id       INTEGER   REFERENCES material (mtr_id) ON DELETE CASCADE
                                                        ON UPDATE CASCADE
                           NOT NULL,
    mtr_name     TEXT (50) UNIQUE
                           NOT NULL,
    mtr_min      INTEGER   NOT NULL,
    mtr_onhand   INTEGER   NOT NULL,
    mtr_unit     TEXT (30) NOT NULL,
    mtr_quantity INTEGER   NOT NULL
);

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               1,
                               1,
                               1,
                               'เมล็ดกาแฟ',
                               20,
                               20,
                               'ถุง',
                               20
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               2,
                               2,
                               2,
                               'ผงชาเขียว',
                               20,
                               15,
                               'ซอง',
                               20
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               3,
                               3,
                               3,
                               'ผงชาไทย',
                               10,
                               12,
                               'ซอง',
                               10
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               4,
                               4,
                               4,
                               'ผงโกโก้',
                               10,
                               25,
                               'ซอง',
                               '-'
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               5,
                               5,
                               5,
                               'ผงช็อกโกแลต',
                               10,
                               18,
                               'ซอง',
                               10
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               6,
                               6,
                               6,
                               'ผงเนสที',
                               10,
                               10,
                               'ซอง',
                               10
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               7,
                               7,
                               7,
                               'ผงแอปเปิ้ล',
                               10,
                               26,
                               'ซอง',
                               '-'
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               8,
                               8,
                               8,
                               'นมหมี',
                               10,
                               6,
                               'กระป๋อง',
                               10
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               9,
                               9,
                               9,
                               'นมสดเมจิ',
                               5,
                               2,
                               'แกลลอน',
                               5
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               10,
                               10,
                               10,
                               'โอรีโอ้',
                               10,
                               5,
                               'ซอง',
                               10
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               11,
                               11,
                               11,
                               'น้ำตาลทราย',
                               10,
                               2,
                               'กิโลกรัม',
                               10
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               12,
                               12,
                               12,
                               'ครีมเทียม',
                               10,
                               3,
                               'ซอง',
                               10
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               13,
                               13,
                               13,
                               'นมข้นจืด',
                               10,
                               2,
                               'กระป๋อง',
                               10
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               14,
                               14,
                               14,
                               'นมข้นหวาน',
                               10,
                               2,
                               'กระป๋อง',
                               10
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               15,
                               15,
                               15,
                               'ไซรับ',
                               3,
                               7,
                               'ขวด',
                               '-'
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               16,
                               16,
                               16,
                               'น้ำเชื่อม',
                               5,
                               2,
                               'ลิตร',
                               5
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               17,
                               17,
                               17,
                               'วิปปิ้งครีม',
                               5,
                               7,
                               'ลิตร',
                               '-'
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               18,
                               18,
                               18,
                               'น้ำส้ม',
                               5,
                               2,
                               'ขวด',
                               5
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               19,
                               19,
                               19,
                               'น้ำแอปเปิ้ล',
                               5,
                               3,
                               'ขวด',
                               5
                           );

INSERT INTO order_material (
                               order_mtr_id,
                               order_id,
                               mtr_id,
                               mtr_name,
                               mtr_min,
                               mtr_onhand,
                               mtr_unit,
                               mtr_quantity
                           )
                           VALUES (
                               20,
                               20,
                               20,
                               'สตรอว์เบอร์รี',
                               5,
                               4,
                               'ขวด',
                               5
                           );


-- Table: order_product
DROP TABLE IF EXISTS order_product;

CREATE TABLE order_product (
    order_product_id    INTEGER   NOT NULL
                                  PRIMARY KEY ASC AUTOINCREMENT,
    order_id            INTEGER   NOT NULL
                                  REFERENCES [order] (order_id) ON DELETE CASCADE
                                                                ON UPDATE CASCADE,
    product_name        TEXT (50) NOT NULL
                                  UNIQUE,
    product_size        TEXT (3)  NOT NULL
                                  DEFAULT SML,
    product_type        TEXT (3)  NOT NULL
                                  DEFAULT HCF,
    product_level       TEXT (3)  NOT NULL
                                  DEFAULT (123),
    amount              DOUBLE    NOT NULL,
    order_product_price DOUBLE    NOT NULL,
    total               DOUBLE    NOT NULL
);

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              1,
                              1,
                              'เอสเปรสโซ',
                              'SML',
                              'HCF',
                              '123',
                              1.0,
                              55.0,
                              55.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              2,
                              2,
                              'แบล็คคอฟฟี่',
                              'SML',
                              'HC',
                              '123',
                              2.0,
                              55.0,
                              110.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              3,
                              3,
                              'แบล็คคอฟฟี่น้ำผึ้ง',
                              'SML',
                              'C',
                              '123',
                              1.0,
                              65.0,
                              65.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              4,
                              4,
                              'คาปูชิโน',
                              'SML',
                              'HCF',
                              '123',
                              1.0,
                              60.0,
                              60.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              5,
                              5,
                              'ลาเต้',
                              'SML',
                              'HCF',
                              '123',
                              3.0,
                              65.0,
                              195.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              6,
                              6,
                              'มอคค่า',
                              'SML',
                              'HCF',
                              '123',
                              1.0,
                              65.0,
                              65.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              7,
                              7,
                              'ชา',
                              'SML',
                              'H',
                              '0',
                              2.0,
                              40.0,
                              80.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              8,
                              8,
                              'ชาเขียวนม',
                              'SML',
                              'HCF',
                              '123',
                              1.0,
                              50.0,
                              50.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              9,
                              9,
                              'ชามะนาว',
                              'SML',
                              'C',
                              '0',
                              2.0,
                              45.0,
                              90.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              10,
                              10,
                              'ชาดำ',
                              'SML',
                              'C',
                              '0',
                              1.0,
                              35.0,
                              35.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              11,
                              11,
                              'โกโก้',
                              'SML',
                              'HCF',
                              '123',
                              1.0,
                              60.0,
                              60.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              12,
                              12,
                              'นมสด',
                              'SML',
                              'HCF',
                              '123',
                              2.0,
                              55.0,
                              110.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              13,
                              13,
                              'ดาร์คช็อกโกแลต',
                              'SML',
                              'HCF',
                              '123',
                              1.0,
                              65.0,
                              65.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              14,
                              14,
                              'อเมริกาโน่',
                              'SML',
                              'HC',
                              '123',
                              2.0,
                              55.0,
                              110.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              15,
                              15,
                              'นมสดชมพู',
                              'SML',
                              'HCF',
                              '123',
                              3.0,
                              55.0,
                              165.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              16,
                              16,
                              'ชาไทย',
                              'SML',
                              'HCF',
                              '123',
                              1.0,
                              60.0,
                              60.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              17,
                              17,
                              'ชาอัญชันนม',
                              'SML',
                              'HCF',
                              '123',
                              2.0,
                              60.0,
                              120.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              18,
                              18,
                              'ชามะลินม',
                              'SML',
                              'HCF',
                              '123',
                              1.0,
                              60.0,
                              60.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              19,
                              19,
                              'ชากุหลาบนม',
                              'SML',
                              'HCF',
                              '123',
                              2.0,
                              60.0,
                              120.0
                          );

INSERT INTO order_product (
                              order_product_id,
                              order_id,
                              product_name,
                              product_size,
                              product_type,
                              product_level,
                              amount,
                              order_product_price,
                              total
                          )
                          VALUES (
                              20,
                              20,
                              'ชานมไต้หวัน',
                              'SML',
                              'HCF',
                              '123',
                              1.0,
                              60.0,
                              60.0
                          );


-- Table: order_report
DROP TABLE IF EXISTS order_report;

CREATE TABLE order_report (
    order_id    INTEGER PRIMARY KEY ASC AUTOINCREMENT
                        NOT NULL,
    store_id    INTEGER NOT NULL
                        REFERENCES store (store_id) ON DELETE CASCADE
                                                    ON UPDATE CASCADE,
    emp_id      INTEGER REFERENCES employee (emp_id) ON DELETE CASCADE
                                                     ON UPDATE CASCADE
                        NOT NULL,
    report_date DATE    NOT NULL
);

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             1,
                             1,
                             1,
                             '2565/9/10'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             2,
                             2,
                             2,
                             '2565/10/16'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             3,
                             3,
                             3,
                             '2565/10/23'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             4,
                             4,
                             4,
                             '2565/10/30'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             5,
                             5,
                             5,
                             '2565/6/11'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             6,
                             6,
                             6,
                             '2565/11/13'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             7,
                             7,
                             7,
                             '2565/11/20'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             8,
                             8,
                             8,
                             '2565/11/20'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             9,
                             9,
                             9,
                             '2565/12/04'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             10,
                             10,
                             10,
                             '2565/11/12'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             11,
                             11,
                             11,
                             '2565/12/18'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             12,
                             12,
                             12,
                             '2565/12/25'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             13,
                             13,
                             13,
                             '2566/1/01'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             14,
                             14,
                             14,
                             '2566/8/01'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             15,
                             15,
                             15,
                             '2566/1/15'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             16,
                             16,
                             16,
                             '2566/1/22'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             17,
                             17,
                             17,
                             '2566/1/29'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             18,
                             18,
                             18,
                             '2566/5/02'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             19,
                             19,
                             19,
                             '2566/2/12'
                         );

INSERT INTO order_report (
                             order_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             20,
                             20,
                             20,
                             '2566/2/19'
                         );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    product_id    INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    product_name  TEXT (50) UNIQUE
                            NOT NULL,
    product_size  TEXT (3)  NOT NULL
                            DEFAULT SML,
    product_type  TEXT (3)  NOT NULL
                            DEFAULT HCF,
    product_level TEXT (3)  NOT NULL
                            DEFAULT (123),
    product_price DOUBLE    NOT NULL,
    product_image TEXT (50) NOT NULL
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        1,
                        'เอสเปรสโซ',
                        'SML',
                        'HCF',
                        '123',
                        55.0,
                        'expresso.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        2,
                        'แบล็คคอฟฟี่',
                        'SML',
                        'HC',
                        '123',
                        55.0,
                        'blackcoffee.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        3,
                        'แบล็คคอฟฟี่น้ำผึ้ง',
                        'SML',
                        'C',
                        '123',
                        65.0,
                        'honeyblackcoffee.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        4,
                        'คาปูชิโน',
                        'SML',
                        'HCF',
                        '123',
                        60.0,
                        'cappucino.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        5,
                        'ลาเต้',
                        'SML',
                        'HCF',
                        '123',
                        65.0,
                        'latte.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        6,
                        'มอคค่า',
                        'SML',
                        'HCF',
                        '123',
                        65.0,
                        'moccha.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        7,
                        'ชา',
                        'SML',
                        'H',
                        '0',
                        40.0,
                        'tea.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        8,
                        'ชาเขียวนม',
                        'SML',
                        'HCF',
                        '123',
                        50.0,
                        'greenteawithmilk.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        9,
                        'ชามะนาว',
                        'SML',
                        'C',
                        '0',
                        45.0,
                        'lemontea.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        10,
                        'ชาดำ',
                        'SML',
                        'C',
                        '0',
                        35.0,
                        'blacktea.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        11,
                        'โกโก้',
                        'SML',
                        'HCF',
                        '123',
                        60.0,
                        'cocoa.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        12,
                        'นมสด',
                        'SML',
                        'HCF',
                        '123',
                        55.0,
                        'milk.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        13,
                        'ดาร์คช็อกโกแลต',
                        'SML',
                        'HCF',
                        '123',
                        65.0,
                        'darkchocolate.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        14,
                        'อเมริกาโน่',
                        'SML',
                        'HC',
                        '123',
                        55.0,
                        'americano.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        15,
                        'นมสดชมพู',
                        'SML',
                        'HCF',
                        '123',
                        55.0,
                        'pinkmilk.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        16,
                        'ชาไทย',
                        'SML',
                        'HCF',
                        '123',
                        60.0,
                        'thaitea.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        17,
                        'ชาอัญชันนม',
                        'SML',
                        'HCF',
                        '123',
                        60.0,
                        'butterflypeamilk.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        18,
                        'ชามะลินม',
                        'SML',
                        'HCF',
                        '123',
                        60.0,
                        'milkjasminetea.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        19,
                        'ชากุหลาบนม',
                        'SML',
                        'HCF',
                        '123',
                        60.0,
                        'milkerosetea.png'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_size,
                        product_type,
                        product_level,
                        product_price,
                        product_image
                    )
                    VALUES (
                        20,
                        'ชานมไต้หวัน',
                        'SML',
                        'HCF',
                        '123',
                        60.0,
                        'taiwantea.png'
                    );


-- Table: report_material
DROP TABLE IF EXISTS report_material;

CREATE TABLE report_material (
    report_mtr_id INTEGER   NOT NULL
                            PRIMARY KEY ASC AUTOINCREMENT,
    report_id     INTEGER   NOT NULL
                            REFERENCES stock_report (report_id) ON DELETE CASCADE
                                                                ON UPDATE CASCADE,
    mtr_id        INTEGER   REFERENCES material (mtr_id) ON DELETE CASCADE
                                                         ON UPDATE CASCADE
                            NOT NULL,
    mtr_name      TEXT (50) UNIQUE
                            NOT NULL,
    mtr_min       INTEGER   NOT NULL,
    mtr_onhand    INTEGER   NOT NULL,
    mtr_unit      TEXT (15) NOT NULL,
    mtr_quantity  INTEGER   NOT NULL
);

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                1,
                                1,
                                1,
                                'เมล็ดกาแฟ',
                                20,
                                20,
                                'ถุง',
                                15
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                2,
                                2,
                                2,
                                'ผงชาเขียว',
                                20,
                                15,
                                'ซอง',
                                25
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                3,
                                3,
                                3,
                                'ผงชาไทย',
                                10,
                                12,
                                'ซอง',
                                20
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                4,
                                4,
                                4,
                                'ผงโกโก้',
                                10,
                                25,
                                'ซอง',
                                '-'
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                5,
                                5,
                                5,
                                'ผงช็อกโกแลต',
                                10,
                                18,
                                'ซอง',
                                22
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                6,
                                6,
                                6,
                                'ผงเนสที',
                                10,
                                10,
                                'ซอง',
                                20
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                7,
                                7,
                                7,
                                'ผงแอปเปิ้ล',
                                10,
                                26,
                                'ซอง',
                                4
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                8,
                                8,
                                8,
                                'นมหมี',
                                10,
                                6,
                                'กระป๋อง',
                                14
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                9,
                                9,
                                9,
                                'นมสดเมจิ',
                                5,
                                2,
                                'แกลลอน',
                                8
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                10,
                                10,
                                10,
                                'โอรีโอ้',
                                10,
                                5,
                                'ซอง',
                                15
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                11,
                                11,
                                11,
                                'น้ำตาลทราย',
                                10,
                                2,
                                'กิโลกรัม',
                                18
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                12,
                                12,
                                12,
                                'ครีมเทียม',
                                10,
                                3,
                                'ซอง',
                                17
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                13,
                                13,
                                13,
                                'นมข้นจืด',
                                10,
                                2,
                                'กระป๋อง',
                                18
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                14,
                                14,
                                14,
                                'นมข้นหวาน',
                                10,
                                2,
                                'กระป๋อง',
                                18
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                15,
                                15,
                                15,
                                'ไซรับ',
                                3,
                                7,
                                'ขวด',
                                13
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                16,
                                16,
                                16,
                                'น้ำเชื่อม',
                                5,
                                2,
                                'ลิตร',
                                15
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                17,
                                17,
                                17,
                                'วิปปิ้งครีม',
                                5,
                                7,
                                'ลิตร',
                                13
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                18,
                                18,
                                18,
                                'น้ำส้ม',
                                5,
                                2,
                                'ขวด',
                                13
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                19,
                                19,
                                19,
                                'น้ำแอปเปิ้ล',
                                5,
                                3,
                                'ขวด',
                                7
                            );

INSERT INTO report_material (
                                report_mtr_id,
                                report_id,
                                mtr_id,
                                mtr_name,
                                mtr_min,
                                mtr_onhand,
                                mtr_unit,
                                mtr_quantity
                            )
                            VALUES (
                                20,
                                20,
                                20,
                                'สตรอว์เบอร์รี',
                                5,
                                4,
                                'ขวด',
                                6
                            );


-- Table: stock_report
DROP TABLE IF EXISTS stock_report;

CREATE TABLE stock_report (
    report_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                          NOT NULL,
    store_id    INTEGER   NOT NULL
                          REFERENCES store (store_id) ON DELETE CASCADE
                                                      ON UPDATE CASCADE,
    emp_id      INTEGER   NOT NULL
                          REFERENCES employee (emp_id) ON DELETE CASCADE
                                                       ON UPDATE CASCADE,
    report_date DATE (10) NOT NULL
);

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             1,
                             1,
                             1,
                             '2565/9/10'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             2,
                             2,
                             2,
                             '2565/10/16'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             3,
                             3,
                             3,
                             '2565/10/23'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             4,
                             4,
                             4,
                             '2565/10/30'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             5,
                             5,
                             5,
                             '2565/6/11'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             6,
                             6,
                             6,
                             '2565/11/13'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             7,
                             7,
                             7,
                             '2565/10/20'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             8,
                             8,
                             8,
                             '2565/10/27'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             9,
                             9,
                             9,
                             '2565/4/12'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             10,
                             10,
                             10,
                             '2565/11/12'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             11,
                             11,
                             11,
                             '2565/12/18'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             12,
                             12,
                             12,
                             '2565/12/25'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             13,
                             13,
                             13,
                             '2566/1/01'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             14,
                             14,
                             14,
                             '2566/8/01'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             15,
                             15,
                             15,
                             '2566/1/15'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             16,
                             16,
                             16,
                             '2566/1/22'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             17,
                             17,
                             17,
                             '2566/1/29'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             18,
                             18,
                             18,
                             '2566/2/05'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             19,
                             19,
                             19,
                             '2566/2/12'
                         );

INSERT INTO stock_report (
                             report_id,
                             store_id,
                             emp_id,
                             report_date
                         )
                         VALUES (
                             20,
                             20,
                             20,
                             '2566/2/19'
                         );


-- Table: store
DROP TABLE IF EXISTS store;

CREATE TABLE store (
    store_id      INTEGER    PRIMARY KEY AUTOINCREMENT
                             NOT NULL,
    emp_id        INTEGER    REFERENCES employee (emp_id) ON DELETE CASCADE
                                                          ON UPDATE CASCADE
                             NOT NULL,
    store_name    TEXT (100) NOT NULL,
    store_address TEXT (100) NOT NULL
                             UNIQUE,
    store_tel     CHAR (10)  UNIQUE
                             NOT NULL,
    store_tax     NUMERIC    UNIQUE
                             NOT NULL,
    store_logo    TEXT (50)  NOT NULL
);

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      1,
                      1,
                      'D-coffee',
                      '299 หมู่ 6 ถ.บางกรวย อ.ไทรน้อย ต.บางบัวทอง จ.นนทบุรี',
                      '092-111-1111',
                      2123456789123,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      2,
                      2,
                      'D-coffee',
                      '123 ถนนวิภาวดี-รังสิต แขวงจอมพล เขตจตุจักร กรุงเทพ',
                      '092-111-2222',
                      2123456789124,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      3,
                      3,
                      'D-coffee',
                      '90/28 ต.บางพึ่ง อ.พระประแดง จ.สมุทรปราการ',
                      '092-111-3333',
                      2123456789125,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      4,
                      4,
                      'D-coffee',
                      '20/46 หมู่ที่ 1 ซอยเลี่ยงเมืองนนท์ ถนน เลี่ยงเมืองนนท์ ต.บางกระสอ อ.เมืองนนทบุรี',
                      '092-111-4444',
                      2123456789126,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      5,
                      5,
                      'D-coffee',
                      '64/5 ม.6 ซอยลมหวน ถ.พิลึก ต.โคกไม้ลาย อ.เมือง จ.ปราจีนบุรี',
                      '092-111-5555',
                      2123456789127,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      6,
                      6,
                      'D-coffee',
                      '527 หมู่ 9 ต.บางบ่อ อ.บางบ่อ จ.สมุทรปราการ',
                      '092-111-6666',
                      2123456789128,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      7,
                      7,
                      'D-coffee',
                      '612 ปากซอยสุขุมวิท 39 แขวงคลองตันเหนือ เขตวัฒนา กรุงเทพ',
                      '092-111-7777',
                      2123456789129,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      8,
                      8,
                      'D-coffee',
                      '23 ราชดำเนิน แขวงบวรนิเวศ เขตพระนคร กรุงเทพ',
                      '092-111-8888',
                      1234567891211,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      9,
                      9,
                      'D-coffee',
                      '17/4 หมู่ 5 ถนนบำรุงราษฎร์ ต.พิบูลสงคราม อ.เมือง จ.เชียงราย',
                      '092-111-9999',
                      1234567891222,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      10,
                      10,
                      'D-coffee',
                      '99/137 ม.18 ต.คลองหนึ่ง อ.คลองหลวง จ.ปทุมธานี',
                      '092-111-0000',
                      1234567891310,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      11,
                      11,
                      'D-coffee',
                      '112 ซอย 1 หมู่ 10 ต.จันทนิมิต อ.เมืองจันทบุรี จันทบุรี',
                      '092-222-0001',
                      1234567891320,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      12,
                      12,
                      'D-coffee',
                      '56 ต. จันทนิมิต อ.เมืองจันทบุรี จ.จันทบุรี',
                      '092-222-0002',
                      1234567891300,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      13,
                      13,
                      'D-coffee',
                      '201 หมู่ 3 ต.ทุ่งสุขลา อ.ศรีราชา จ.ชลบุรี',
                      '092-222-0003',
                      1234567891340,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      14,
                      14,
                      'D-coffee',
                      '84 ถนน บางแสนล่าง 14/3 ต.แสนสุข อ.เมืองชลบุรี จ.ชลบุรี',
                      '092-222-0004',
                      1234567891350,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      15,
                      15,
                      'D-coffee',
                      'หมู่ 2 157/14 ต.บางสมัคร อ.บางปะกง จ.ฉะเชิงเทรา',
                      '092-222-0005',
                      1234567891360,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      16,
                      16,
                      'D-coffee',
                      '38 หมู่ 4 บ้านหนองนกไข่ ต.หนองนกไข่ อ.กระทุ่มแบน จ.สมุทรสาคร',
                      '092-222-0006',
                      1234567891370,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      17,
                      17,
                      'D-coffee',
                      '533 หมู่ 7 ตำบล ท้ายบ้านใหม่ อ.เมือง จ.สมุทรปราการ',
                      '092-222-0007',
                      3123456789123,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      18,
                      18,
                      'D-coffee',
                      '86/2 ถ.แสงชูโต ต.ท่ามะขาม อ.เมืองกาญจนบุรี จ.กาญจนบุรี',
                      '092-222-0008',
                      1234567891380,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      19,
                      19,
                      'D-coffee',
                      'หมู่ที่ 1 550 ถนนทหาร อ.เมืองอุดรธานี จ.อุดรธานี',
                      '092-222-0009',
                      1234567891390,
                      'D-coffee.jpg'
                  );

INSERT INTO store (
                      store_id,
                      emp_id,
                      store_name,
                      store_address,
                      store_tel,
                      store_tax,
                      store_logo
                  )
                  VALUES (
                      20,
                      20,
                      'D-coffee',
                      '168 หมู่ 4 ตำบลแก่งเสี้ยน อ.เมือง จ.กาญจนบุรี',
                      '092-222-0010',
                      1234567891400,
                      'D-coffee.jpg'
                  );


-- Table: timeclock_report
DROP TABLE IF EXISTS timeclock_report;

CREATE TABLE timeclock_report (
    tc_report_id INTEGER  PRIMARY KEY ASC AUTOINCREMENT
                          NOT NULL,
    store_id     INTEGER  NOT NULL
                          REFERENCES store (store_id) ON DELETE CASCADE
                                                      ON UPDATE CASCADE,
    emp_id       INTEGER  REFERENCES employee (emp_id) ON DELETE CASCADE
                                                       ON UPDATE CASCADE
                          NOT NULL,
    tc_date      DATE     NOT NULL,
    tc_in        DATETIME NOT NULL,
    tc_out       DATETIME NOT NULL,
    tc_hours     INTEGER  NOT NULL
);

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 1,
                                 1,
                                 1,
                                 '2022/1/01',
                                 '2022/1/01 8:00:00',
                                 '2022/1/01 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 2,
                                 1,
                                 2,
                                 '2022/1/02',
                                 '2022/1/02 8:00:00',
                                 '2022/1/02 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 3,
                                 1,
                                 3,
                                 '2022/1/03',
                                 '2022/1/03 8:00:00',
                                 '2022/1/03 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 4,
                                 1,
                                 4,
                                 '2022/1/04',
                                 '2022/1/04 8:00:00',
                                 '2022/1/04 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 5,
                                 1,
                                 5,
                                 '2022/1/05',
                                 '2022/1/05 8:00:00',
                                 '2022/1/05 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 6,
                                 1,
                                 6,
                                 '2022/1/06',
                                 '2022/1/06 8:00:00',
                                 '2022/1/06 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 7,
                                 1,
                                 7,
                                 '2022/1/07',
                                 '2022/1/07 8:00:00',
                                 '2022/1/07 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 8,
                                 1,
                                 8,
                                 '2022/1/08',
                                 '2022/1/08 8:00:00',
                                 '2022/1/08 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 9,
                                 1,
                                 9,
                                 '2022/1/09',
                                 '2022/1/09 8:00:00',
                                 '2022/1/09 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 10,
                                 2,
                                 10,
                                 '2022/1/10',
                                 '2022/1/10 8:00:00',
                                 '2022/1/10 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 11,
                                 2,
                                 11,
                                 '2022/1/11',
                                 '2022/1/11 8:00:00',
                                 '2022/1/11 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 12,
                                 2,
                                 12,
                                 '2022/1/12',
                                 '2022/1/12 8:00:00',
                                 '2022/1/12 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 13,
                                 2,
                                 13,
                                 '2022/1/13',
                                 '2022/1/13 8:00:00',
                                 '2022/1/13 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 14,
                                 2,
                                 14,
                                 '2022/1/14',
                                 '2022/1/14 8:00:00',
                                 '2022/1/14 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 15,
                                 2,
                                 15,
                                 '2022/1/15',
                                 '2022/1/15 8:00:00',
                                 '2022/1/15 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 16,
                                 2,
                                 16,
                                 '2022/1/16',
                                 '2022/1/16 8:00:00',
                                 '2022/1/16 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 17,
                                 2,
                                 17,
                                 '2022/1/17',
                                 '2022/1/17 8:00:00',
                                 '2022/1/17 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 18,
                                 2,
                                 18,
                                 '2022/1/18',
                                 '2022/1/18 8:00:00',
                                 '2022/1/18 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 19,
                                 2,
                                 19,
                                 '2022/1/19',
                                 '2022/1/19 8:00:00',
                                 '2022/1/19 17:00:00',
                                 8
                             );

INSERT INTO timeclock_report (
                                 tc_report_id,
                                 store_id,
                                 emp_id,
                                 tc_date,
                                 tc_in,
                                 tc_out,
                                 tc_hours
                             )
                             VALUES (
                                 20,
                                 2,
                                 20,
                                 '2022/1/20',
                                 '2022/1/20 8:00:00',
                                 '2022/1/20 17:00:00',
                                 8
                             );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
