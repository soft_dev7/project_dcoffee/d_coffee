/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TimeClockReport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bwstx
 */
public class TimeClock {

    private int tcReportId;
    private int storeId;
    private int empId;
    private String tcDate;
    private String tcIn;
    private String tcOut;
    private int tcHours;

    TimeClock(int tcReportId, int storeId, int empId, String tcDate, String tcIn, String tcOut, int tcHours) {
        this.tcReportId = tcReportId;
        this.storeId = storeId;
        this.empId = empId;
        this.tcDate = tcDate;
        this.tcIn = tcIn;
        this.tcOut = tcOut;
        this.tcHours = tcHours;
    }

    TimeClock(int storeId, int empId, String tcDate, String tcIn, String tcOut, int tcHours) {
        this.tcReportId = -1;
        this.storeId = storeId;
        this.empId = empId;
        this.tcDate = tcDate;
        this.tcIn = tcIn;
        this.tcOut = tcOut;
        this.tcHours = tcHours;
    }

    TimeClock() {
        this.tcReportId = 1;
    }

    public int getTcReportId() {
        return tcReportId;
    }

    public void setTcReportId(int tcReportId) {
        this.tcReportId = tcReportId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getTcDate() {
        return tcDate;
    }

    public void setTcDate(String tcDate) {
        this.tcDate = tcDate;
    }

    public String getTcIn() {
        return tcIn;
    }

    public void setTcIn(String tcIn) {
        this.tcIn = tcIn;
    }

    public String getTcOut() {
        return tcOut;
    }

    public void setTcOut(String tcOut) {
        this.tcOut = tcOut;
    }

    public int getTcHours() {
        return tcHours;
    }

    public void setTcHours(int tcHours) {
        this.tcHours = tcHours;
    }

    @Override
    public String toString() {
        return "TimeClock{" + "tcReportId=" + tcReportId + ", storeId=" + storeId + ", empId=" + empId + ", tcDate=" + tcDate + ", tcIn=" + tcIn + ", tcOut=" + tcOut + ", tcHours=" + tcHours + '}';
    }

    public static TimeClock fromRS(ResultSet rs) {
        TimeClock timeClock = new TimeClock();
        try {
            timeClock.setTcReportId(rs.getInt("tc_report_id"));
            timeClock.setStoreId(rs.getInt("store_id"));
            timeClock.setEmpId(rs.getInt("emp_id"));
            timeClock.setTcDate(rs.getString("tc_date"));
            timeClock.setTcIn(rs.getString("tc_in"));
            timeClock.setTcOut(rs.getString("tc_out"));
            timeClock.setTcHours(rs.getInt("tc_hours"));
        } catch (SQLException ex) {
            Logger.getLogger(TimeClock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return timeClock;
    }
}
