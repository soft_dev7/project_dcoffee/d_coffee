/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author KHANOMMECOM
 */
public class InsertDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:coffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        String sql = "INSERT INTO customer(cus_id, cus_name, cus_surname, cus_tel, cus_point, cus_nop, cus_discount) VALUES (?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 21);
            stmt.setString(2, "สุวรรณลักษณ์");
            stmt.setString(3, "หิรัญรักษณา");
            stmt.setString(4, "094-174-5479");
            stmt.setInt(5, 0);
            stmt.setInt(6, 1);
            stmt.setDouble(7, 0.0);
            
            int status = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
