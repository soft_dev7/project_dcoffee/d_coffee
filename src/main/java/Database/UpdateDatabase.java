/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author KHANOMMECOM
 */
public class UpdateDatabase {
    public static void main(String[] args) {
Connection conn = null;
String url = "jdbc:sqlite:coffee.db";
//connection database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLlite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Select
        String sql = "UPDATE  customer SET  cus_name=?, cus_surname=?, cus_tel=?, cus_point=?, cus_nop=?,cus_discount=?WHERE cus_id=?";
        try {
             // obj statement
             PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setString(1, "วินัย");
            stmt.setString(2, "งดงาม");
            stmt.setString(3, "062-222-5566");
            stmt.setInt(4, 100);
            stmt.setInt(5, 20);
            stmt.setDouble(6, 5);
            stmt.setInt(7, 1);
            
        int status = stmt.executeUpdate();
//            ResultSet key = stmt.getGeneratedKeys();
//           key.next();
            
//         System.out.println(""+key.getInt(1));
            
        } catch (SQLException ex) { //error
            System.out.println(ex.getMessage());
        }

//close database     
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }
}
