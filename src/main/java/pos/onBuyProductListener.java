/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package pos;

import model.Product;


/**
 *
 * @author Acer
 */
public interface onBuyProductListener {
    public void buy(Product product,int amount,int sweetLevel, String type);
}
