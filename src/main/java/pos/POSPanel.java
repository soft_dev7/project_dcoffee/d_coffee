/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package pos;

import Service.CustomerService;
import Service.OrderProductService;
import Service.OrderService;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import mainPage.mainPage;
import model.Customer;
import model.Employee;
import model.Order;
import model.OrderProduct;
import model.Product;

/**
 *
 * @author Acer
 */
public class POSPanel extends javax.swing.JPanel {

    /**
     * Creates new form posPanel
     */
    Customer updatedCustomer;
    Customer customer;
    Employee employee;
    CustomerService customerService = new CustomerService();
    OrderProductService orderProductService = new OrderProductService();
    DefaultTableModel model;
    private ProductListPanel productListPanel = new ProductListPanel();
    private mainPage main = new mainPage();
    private OrderService orderServive = new OrderService();
    private int currentOrderID = orderServive.getLast().getOrderId() + 1;
    private int currentOrderProductID = orderProductService.getLast().getOrderProductId() + 1;
    private final int storeID = 1;

    public POSPanel() {
        initComponents();
        getDateTime();
        tblProductList.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 11));
        tblProductList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        productListPanel = new ProductListPanel();
        scpProduct.setViewportView(productListPanel);
        productListPanel.setPOSPanel(this);
        main.setPOSPanel(this);
        UIManager.put("OptionPane.messageFont", new Font("Tahoma", Font.BOLD, 14));
        model = (DefaultTableModel) tblProductList.getModel();
        enableFunction(false);

        txtDiscount.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                getItemCost();
                if (txtReceive.getText().isBlank() == false) {
                    getChange();
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                getItemCost();
                if (txtReceive.getText().isBlank() == false) {
                    getChange();
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                getItemCost();
                if (txtReceive.getText().isBlank() == false) {
                    getChange();
                }
            }
        });

        txtCusPointUse.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if (customer != null && model.getRowCount() > 0) {
                    if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > customer.getCusPoint() + 1) {
                        setTxtCustomerPointUse(customer.getCusPoint() + 1);
                    } else if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > 20) {
                        setTxtCustomerPointUse(20);
                    }
                    txtCusPointRemain.setText(Integer.toString(getCustomerPointRemain()));
                    txtMemberDiscount.setText(Double.toString(getCustomerPointUse() / 2));
                    getItemCost();
                    if (txtReceive.getText().isBlank() == false) {
                        getChange();
                    }
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (customer != null && model.getRowCount() > 0) {
                    if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > customer.getCusPoint() + 1) {
                        setTxtCustomerPointUse(customer.getCusPoint() + 1);
                    } else if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > 20) {
                        setTxtCustomerPointUse(20);
                    }
                    txtCusPointRemain.setText(Integer.toString(getCustomerPointRemain()));
                    txtMemberDiscount.setText(Double.toString(getCustomerPointUse() / 2));
                    getItemCost();
                    if (txtReceive.getText().isBlank() == false) {
                        getChange();
                    }
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if (customer != null && model.getRowCount() > 0) {
                    if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > customer.getCusPoint() + 1) {
                        setTxtCustomerPointUse(customer.getCusPoint() + 1);
                    } else if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > 20) {
                        setTxtCustomerPointUse(20);
                    }
                    txtCusPointRemain.setText(Integer.toString(getCustomerPointRemain()));
                    txtMemberDiscount.setText(Double.toString(getCustomerPointUse() / 2));
                    getItemCost();
                    if (txtReceive.getText().isBlank() == false) {
                        getChange();
                    }
                }
            }
        });

        txtReceive.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if (model.getRowCount() > 0 && !txtReceive.getText().isBlank()) {
                    getChange();
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (model.getRowCount() > 0 && !txtReceive.getText().isBlank()) {
                    getChange();
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if (model.getRowCount() > 0 && !txtReceive.getText().isBlank()) {
                    getChange();
                }
            }
        });
    }

    public void getDateTime() {
        txtDateTime.setEditable(false);
        Timer timer;
        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date date = new Date();
                DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                String time = timeFormat.format(date);

                Date date2 = new Date();
                DateFormat timeFormat2 = new SimpleDateFormat("dd-MM-yyyy");
                String time2 = timeFormat2.format(date2);
                txtDateTime.setText(time2 + " " + time);
            }
        };
        timer = new Timer(1000, actionListener);
        timer.setInitialDelay(0);
        timer.start();
    }

    public void setEmployeeFromMainPage(Employee employee) {
        this.employee = employee;
    }

    public String dateTimeNow() {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);

    }

    public Order orderDetail() {
        Order order = new Order();
        order.setOrderId(currentOrderID);
        order.setOrderDateTime(dateTimeNow());
        order.setStoreId(storeID);

        int cusid = -1;
        if (customer != null) {
            cusid = customer.getCusId();
        }

        order.setCusId(cusid);

        int empId = -1;
        if (employee != null) {
            empId = employee.getEmpId();
        }
        order.setEmpId(empId);

        double total = 0;
        if (txtTotal1.getText().isBlank() == false) {
            total = Double.parseDouble(txtTotal1.getText());
        }
        order.setOrderTotal(total);

        double discount = 0;
        double memberDiscount = 0;
        if (txtDiscount.getText().isBlank() == false) {
            discount = Double.parseDouble(txtDiscount.getText());
        }
        if (txtMemberDiscount.getText().isBlank() == false) {
            memberDiscount = Double.parseDouble(txtMemberDiscount.getText());
        }

        double totalDiscount = discount + memberDiscount;
        order.setOrderDiscount(totalDiscount);

        double receive = 0;
        if (txtReceive.getText().isBlank() == false) {
            receive = Double.parseDouble(txtReceive.getText());
        }
        order.setOrderCash(receive);

        double change = 0;
        if (txtChange.getText().isBlank() == false) {
            change = Double.parseDouble(txtChange.getText());
        }
        order.setOrderChange(change);

        return order;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        updatedCustomer = customer;
        enableCustomerPointUseTxt();
        showCustomer(customer);
    }

    public void showCustomer(Customer customer) {
        txtCusNOP.setText(Integer.toString(customer.getCusPurchaseCount()));
        txtCusPoint.setText(Integer.toString(customer.getCusPoint()));
        if (model.getRowCount() > 0) {
            txtCusPointReceive.setText("1");
        } else {
            txtCusPointReceive.setText("0");
        }
        if (txtCusPointReceive.getText().equals("1")) {
            txtCusPointRemain.setText(Integer.toString(customer.getCusPoint() + 1));
        } else {
            txtCusPointRemain.setText(Integer.toString(customer.getCusPoint()));
        }
    }

    public void setOrderTable(Product product, int amount, int sweetLevel, String type) {
        lblOrder.setText("รายการที่ " + currentOrderID);
        int price = (int) product.getPrdPrice();
        if (type.equals("ร้อน")) {
            price = price - 5;
        } else if (type.equals("ปั่น")) {
            price = price + 5;
        }

        switch (sweetLevel) {
            case 1 ->
                model.addRow(new Object[]{model.getRowCount() + 1, product.getPrdName(), type, "น้อย", amount, price * amount});
            case 2 ->
                model.addRow(new Object[]{model.getRowCount() + 1, product.getPrdName(), type, "ปกติ", amount, price * amount});
            case 3 ->
                model.addRow(new Object[]{model.getRowCount() + 1, product.getPrdName(), type, "มาก", amount, price * amount});
            default -> {
            }
        }

        if (model.getRowCount() > 0) {
            enableFunction(true);
            if (customer != null) {
                txtCusPointReceive.setText("1");
                txtCusPointUse.setEditable(true);
                txtCusPointRemain.setText(Integer.toString(getCustomerPointRemain()));
            }
        } else {
            enableFunction(false);
            if (customer != null) {
                txtCusPointReceive.setText("0");
                txtCusPointRemain.setText(Integer.toString(getCustomerPointRemain()));
            }
        }

        getItemCost();
    }

    public OrderProduct getOrderProductFromTable(int row) {
        OrderProduct orderProduct = new OrderProduct();
        for (int i = 0; i < tblProductList.getColumnCount(); i++) {
            String value = tblProductList.getValueAt(row, i).toString();
            if (value.equals("เย็น")) {
                value = "C";
            } else if (value.equals("ร้อน")) {
                value = "H";
            } else if (value.equals("ปั่น")) {
                value = "F";
            } else if (value.equals("น้อย")) {
                value = "1";
            } else if (value.equals("ปกติ")) {
                value = "2";
            } else if (value.equals("มาก")) {
                value = "3";
            }
            if (i == 1) {
                orderProduct.setOrderProductId(currentOrderProductID);
                orderProduct.setOrderId(currentOrderID);
                orderProduct.setProductName(value);
            } else if (i == 2) {
                orderProduct.setProductType(value);
            } else if (i == 3) {
                orderProduct.setProductSweetness(value);
            } else if (i == 4) {
                orderProduct.setProductAmount(Integer.parseInt(value));
            } else if (i == 5) {
                orderProduct.setProductPrice(Double.parseDouble(value) / orderProduct.getProductAmount());
                orderProduct.setTotal(orderProduct.getProductPrice() * orderProduct.getProductAmount());
            }
        }

        return orderProduct;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scpProduct = new javax.swing.JScrollPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        pnlTotal2 = new javax.swing.JPanel();
        lblTotal3 = new javax.swing.JLabel();
        txtTotal1 = new javax.swing.JTextField();
        pnlOrder = new javax.swing.JPanel();
        scpProductList = new javax.swing.JScrollPane();
        tblProductList = new javax.swing.JTable();
        lblOrder = new javax.swing.JLabel();
        btnDecrease = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        btnIncrease = new javax.swing.JButton();
        pnlTotal1 = new javax.swing.JPanel();
        lblTotal = new javax.swing.JLabel();
        lblSubtotal = new javax.swing.JLabel();
        lblMemberDiscount = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        txtSubtotal = new javax.swing.JTextField();
        txtMemberDiscount = new javax.swing.JTextField();
        txtDiscount = new javax.swing.JTextField();
        txtTax = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        lblTax1 = new javax.swing.JLabel();
        lblPoints = new javax.swing.JLabel();
        lblNOP = new javax.swing.JLabel();
        lblPoint1 = new javax.swing.JLabel();
        lblPointReceive1 = new javax.swing.JLabel();
        lblPointUse1 = new javax.swing.JLabel();
        lblPointRemain1 = new javax.swing.JLabel();
        txtCusPoint = new javax.swing.JTextField();
        txtCusPointReceive = new javax.swing.JTextField();
        txtCusPointUse = new javax.swing.JTextField();
        txtCusPointRemain = new javax.swing.JTextField();
        txtCusNOP = new javax.swing.JTextField();
        lblChange = new javax.swing.JLabel();
        lblReceive = new javax.swing.JLabel();
        txtChange = new javax.swing.JTextField();
        txtReceive = new javax.swing.JTextField();
        btnPay = new javax.swing.JButton();
        btnMemberReg = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btnSearchMember = new javax.swing.JButton();
        combPayMethod = new javax.swing.JComboBox<>();
        lblTotal2 = new javax.swing.JLabel();
        txtDateTime = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));

        scpProduct.setBackground(new java.awt.Color(211, 197, 186));
        scpProduct.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        scpProduct.setForeground(new java.awt.Color(211, 197, 186));

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane2.setBorder(null);
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        pnlTotal2.setBackground(new java.awt.Color(211, 197, 186));
        pnlTotal2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        pnlTotal2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTotal3.setFont(new java.awt.Font("Tahoma", 1, 26)); // NOI18N
        lblTotal3.setForeground(new java.awt.Color(84, 70, 63));
        lblTotal3.setText("รวม");
        pnlTotal2.add(lblTotal3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        txtTotal1.setBackground(new java.awt.Color(211, 197, 186));
        txtTotal1.setFont(new java.awt.Font("Tahoma", 1, 22)); // NOI18N
        txtTotal1.setForeground(new java.awt.Color(84, 70, 63));
        txtTotal1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotal1.setBorder(null);
        txtTotal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotal1ActionPerformed(evt);
            }
        });
        pnlTotal2.add(txtTotal1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 200, 30));

        pnlOrder.setBackground(new java.awt.Color(211, 197, 186));
        pnlOrder.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        pnlOrder.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblProductList.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblProductList.setForeground(new java.awt.Color(84, 70, 63));
        tblProductList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ลำดับ", "สินค้า", "ประเภท", "ความหวาน", "จำนวน", "ราคา"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblProductList.setSelectionBackground(new java.awt.Color(84, 70, 63));
        tblProductList.setSelectionForeground(new java.awt.Color(255, 255, 255));
        scpProductList.setViewportView(tblProductList);

        pnlOrder.add(scpProductList, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 400, 230));

        lblOrder.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrder.setForeground(new java.awt.Color(84, 70, 63));
        lblOrder.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOrder.setText("รายการ");
        pnlOrder.add(lblOrder, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 10, 420, -1));

        btnDecrease.setBackground(new java.awt.Color(84, 70, 63));
        btnDecrease.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnDecrease.setForeground(new java.awt.Color(255, 255, 255));
        btnDecrease.setText("-");
        btnDecrease.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDecrease.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDecreaseActionPerformed(evt);
            }
        });
        pnlOrder.add(btnDecrease, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 290, 40, 30));

        btnRemove.setBackground(new java.awt.Color(84, 70, 63));
        btnRemove.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnRemove.setForeground(new java.awt.Color(255, 255, 255));
        btnRemove.setText("ยกเลิกรายการ");
        btnRemove.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        pnlOrder.add(btnRemove, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 290, 100, 30));

        btnIncrease.setBackground(new java.awt.Color(84, 70, 63));
        btnIncrease.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnIncrease.setForeground(new java.awt.Color(255, 255, 255));
        btnIncrease.setText("+");
        btnIncrease.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnIncrease.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncreaseActionPerformed(evt);
            }
        });
        pnlOrder.add(btnIncrease, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 40, 30));

        pnlTotal1.setBackground(new java.awt.Color(211, 197, 186));
        pnlTotal1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        pnlTotal1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(84, 70, 63));
        lblTotal.setText("รวม :");
        pnlTotal1.add(lblTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, -1, -1));

        lblSubtotal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblSubtotal.setForeground(new java.awt.Color(84, 70, 63));
        lblSubtotal.setText("ราคารวม :");
        pnlTotal1.add(lblSubtotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        lblMemberDiscount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblMemberDiscount.setForeground(new java.awt.Color(84, 70, 63));
        lblMemberDiscount.setText("ส่วนลดสมาชิก :");
        pnlTotal1.add(lblMemberDiscount, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, -1, -1));

        lblDiscount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblDiscount.setForeground(new java.awt.Color(84, 70, 63));
        lblDiscount.setText("ส่วนลด :");
        pnlTotal1.add(lblDiscount, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, -1, -1));

        txtTotal.setBackground(new java.awt.Color(211, 197, 186));
        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 22)); // NOI18N
        txtTotal.setForeground(new java.awt.Color(84, 70, 63));
        txtTotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotal.setBorder(null);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        pnlTotal1.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 150, 270, 30));

        txtSubtotal.setBackground(new java.awt.Color(211, 197, 186));
        txtSubtotal.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtSubtotal.setForeground(new java.awt.Color(84, 70, 63));
        txtSubtotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSubtotal.setBorder(null);
        pnlTotal1.add(txtSubtotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 10, 270, -1));

        txtMemberDiscount.setBackground(new java.awt.Color(211, 197, 186));
        txtMemberDiscount.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtMemberDiscount.setForeground(new java.awt.Color(84, 70, 63));
        txtMemberDiscount.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMemberDiscount.setBorder(null);
        pnlTotal1.add(txtMemberDiscount, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 40, 270, -1));

        txtDiscount.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtDiscount.setForeground(new java.awt.Color(84, 70, 63));
        txtDiscount.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDiscount.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        txtDiscount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtDiscountMouseExited(evt);
            }
        });
        txtDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountActionPerformed(evt);
            }
        });
        txtDiscount.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                txtDiscountPropertyChange(evt);
            }
        });
        txtDiscount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDiscountKeyTyped(evt);
            }
        });
        pnlTotal1.add(txtDiscount, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, 270, -1));

        txtTax.setBackground(new java.awt.Color(211, 197, 186));
        txtTax.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtTax.setForeground(new java.awt.Color(84, 70, 63));
        txtTax.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTax.setBorder(null);
        pnlTotal1.add(txtTax, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 100, 270, -1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(84, 70, 63));
        jLabel1.setText("_________________________________________________________");
        pnlTotal1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 420, 30));

        lblTax1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblTax1.setForeground(new java.awt.Color(84, 70, 63));
        lblTax1.setText("ภาษีมูลค่าเพิ่ม :");
        pnlTotal1.add(lblTax1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, -1, -1));

        lblPoints.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblPoints.setForeground(new java.awt.Color(84, 70, 63));
        lblPoints.setText("แต้ม");
        pnlTotal1.add(lblPoints, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 230, -1, -1));

        lblNOP.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblNOP.setForeground(new java.awt.Color(84, 70, 63));
        lblNOP.setText("จำนวนครั้งที่ซื้อ");
        pnlTotal1.add(lblNOP, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, -1, -1));

        lblPoint1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPoint1.setForeground(new java.awt.Color(84, 70, 63));
        lblPoint1.setText("จำนวนแต้ม");
        pnlTotal1.add(lblPoint1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 200, -1, -1));

        lblPointReceive1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPointReceive1.setForeground(new java.awt.Color(84, 70, 63));
        lblPointReceive1.setText("ได้รับ");
        pnlTotal1.add(lblPointReceive1, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 200, -1, -1));

        lblPointUse1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPointUse1.setForeground(new java.awt.Color(84, 70, 63));
        lblPointUse1.setText("ใช้ไป");
        pnlTotal1.add(lblPointUse1, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 200, -1, -1));

        lblPointRemain1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPointRemain1.setForeground(new java.awt.Color(84, 70, 63));
        lblPointRemain1.setText("คงเหลือ");
        pnlTotal1.add(lblPointRemain1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 200, -1, -1));

        txtCusPoint.setBackground(new java.awt.Color(211, 197, 186));
        txtCusPoint.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtCusPoint.setForeground(new java.awt.Color(84, 70, 63));
        txtCusPoint.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCusPoint.setBorder(null);
        pnlTotal1.add(txtCusPoint, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 230, 50, -1));

        txtCusPointReceive.setBackground(new java.awt.Color(211, 197, 186));
        txtCusPointReceive.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtCusPointReceive.setForeground(new java.awt.Color(84, 70, 63));
        txtCusPointReceive.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCusPointReceive.setBorder(null);
        pnlTotal1.add(txtCusPointReceive, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 230, 40, -1));

        txtCusPointUse.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtCusPointUse.setForeground(new java.awt.Color(84, 70, 63));
        txtCusPointUse.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCusPointUse.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        txtCusPointUse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCusPointUseActionPerformed(evt);
            }
        });
        pnlTotal1.add(txtCusPointUse, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 230, 40, -1));

        txtCusPointRemain.setBackground(new java.awt.Color(211, 197, 186));
        txtCusPointRemain.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtCusPointRemain.setForeground(new java.awt.Color(84, 70, 63));
        txtCusPointRemain.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCusPointRemain.setBorder(null);
        pnlTotal1.add(txtCusPointRemain, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 230, 50, -1));

        txtCusNOP.setBackground(new java.awt.Color(211, 197, 186));
        txtCusNOP.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtCusNOP.setForeground(new java.awt.Color(84, 70, 63));
        txtCusNOP.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCusNOP.setBorder(null);
        txtCusNOP.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        pnlTotal1.add(txtCusNOP, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 230, 50, -1));

        lblChange.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblChange.setForeground(new java.awt.Color(84, 70, 63));
        lblChange.setText("จำนวนเงินทอน");

        lblReceive.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblReceive.setForeground(new java.awt.Color(84, 70, 63));
        lblReceive.setText("จำนวนเงินที่รับ");

        txtChange.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtChange.setForeground(new java.awt.Color(84, 70, 63));
        txtChange.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtChange.setBorder(null);
        txtChange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtChangeActionPerformed(evt);
            }
        });

        txtReceive.setBackground(new java.awt.Color(211, 197, 186));
        txtReceive.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtReceive.setForeground(new java.awt.Color(84, 70, 63));
        txtReceive.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtReceive.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        txtReceive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtReceiveActionPerformed(evt);
            }
        });

        btnPay.setBackground(new java.awt.Color(84, 70, 63));
        btnPay.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPay.setForeground(new java.awt.Color(255, 255, 255));
        btnPay.setText("ชำระเงิน");
        btnPay.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPayActionPerformed(evt);
            }
        });

        btnMemberReg.setBackground(new java.awt.Color(84, 70, 63));
        btnMemberReg.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnMemberReg.setForeground(new java.awt.Color(255, 255, 255));
        btnMemberReg.setText("สมัครสมาชิก");
        btnMemberReg.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnMemberReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMemberRegActionPerformed(evt);
            }
        });

        btnPrint.setBackground(new java.awt.Color(84, 70, 63));
        btnPrint.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnPrint.setForeground(new java.awt.Color(255, 255, 255));
        btnPrint.setText("พิมพ์");
        btnPrint.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(84, 70, 63));
        btnCancel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(255, 255, 255));
        btnCancel.setText("ยกเลิกบิล");
        btnCancel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnSearchMember.setBackground(new java.awt.Color(84, 70, 63));
        btnSearchMember.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnSearchMember.setForeground(new java.awt.Color(255, 255, 255));
        btnSearchMember.setText("ค้นหาสมาชิก");
        btnSearchMember.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSearchMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchMemberActionPerformed(evt);
            }
        });

        combPayMethod.setBackground(new java.awt.Color(84, 70, 63));
        combPayMethod.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        combPayMethod.setForeground(new java.awt.Color(255, 255, 255));
        combPayMethod.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "เงินสด", "บัตรเครดิต / เดบิต", "พร้อมเพย์" }));
        combPayMethod.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        combPayMethod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                combPayMethodActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(pnlTotal2, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btnSearchMember, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnMemberReg, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(combPayMethod, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblReceive)
                                .addGap(12, 12, 12)
                                .addComponent(txtReceive, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblChange)
                                .addGap(9, 9, 9)
                                .addComponent(txtChange, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(10, 10, 10))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(pnlOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(pnlTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(10, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(614, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlTotal2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSearchMember, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnMemberReg, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addComponent(combPayMethod, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblReceive))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(txtReceive, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblChange))
                    .addComponent(txtChange, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(106, 106, 106))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addComponent(pnlOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(324, 324, 324)))
        );

        jScrollPane2.setViewportView(jPanel2);

        lblTotal2.setFont(new java.awt.Font("Tahoma", 1, 26)); // NOI18N
        lblTotal2.setForeground(new java.awt.Color(84, 70, 63));
        lblTotal2.setText("รายการ");

        txtDateTime.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtDateTime.setForeground(new java.awt.Color(84, 70, 63));
        txtDateTime.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDateTime.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        txtDateTime.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDateTimeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(scpProduct, javax.swing.GroupLayout.DEFAULT_SIZE, 485, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtDateTime, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblTotal2))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDateTime, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTotal2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(scpProduct)
                        .addGap(10, 10, 10))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2)
                        .addGap(12, 12, 12))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void combPayMethodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_combPayMethodActionPerformed
        if (combPayMethod.getSelectedItem().toString().equals("เงินสด")) {
            txtReceive.setEditable(true);
        } else if (combPayMethod.getSelectedItem().toString().equals("บัตรเครดิต / เดบิต")) {
            txtReceive.setEditable(false);
            txtReceive.setText("");
            txtChange.setText("");
        } else if (combPayMethod.getSelectedItem().toString().equals("พร้อมเพย์")) {
            txtReceive.setEditable(false);
            txtReceive.setText("");
            txtChange.setText("");
        }
    }//GEN-LAST:event_combPayMethodActionPerformed

    private void btnSearchMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchMemberActionPerformed
        SearchCustomerForm searchCustomerForm = new SearchCustomerForm();
        searchCustomerForm.setPOSPanel(this);
        searchCustomerForm.setResizable(false);
        searchCustomerForm.setLocationRelativeTo(null);
        searchCustomerForm.setMinimumSize(null);
        searchCustomerForm.setVisible(true);
    }//GEN-LAST:event_btnSearchMemberActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        if (isTableisEmpty() == false || customer != null) {
            int result = JOptionPane.showConfirmDialog(null, "ต้องการยกเลิกรายการปัจจุบันหรือไม่ ?", "ยืนยันการยกเลิกรายการ",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (result == JOptionPane.YES_OPTION) {
                enableFunction(false);
                if (customer != null) {
                    txtCusPointReceive.setText("0");
                }
                clearCustomerDetail();
            }
        } else {
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f, "ไม่สามารถยกเลิกบิลปัจจุบันได้ เนื่องจากรายการยังว่างอยู่", "ไม่สามารถยกเลิกบิลได้", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnMemberRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMemberRegActionPerformed
        CreateCustomerFrame createCustomerForm = new CreateCustomerFrame();
        createCustomerForm.setResizable(false);
        createCustomerForm.setLocationRelativeTo(null);
        createCustomerForm.setMinimumSize(null);
        createCustomerForm.setVisible(true);
    }//GEN-LAST:event_btnMemberRegActionPerformed

    private void btnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPayActionPerformed
        if (tblProductList.getRowCount() > 0) {
            if (combPayMethod.getSelectedItem().toString().equals("เงินสด")) {
                if (txtReceive.getText().isBlank()) {
                    JOptionPane.showMessageDialog(null, "ไม่สามารถดำเนินการต่อได้ โปรดกรอกจำนวนเงินที่รับมา", "ไม่สามารถดำเนินการต่อได้", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    getChange();
                    int result = JOptionPane.showConfirmDialog(null, "จำนวนเงินที่รับมา : " + Double.parseDouble(txtReceive.getText()) + "\nจำนวนเงินทอน : " + Double.parseDouble(txtChange.getText()) + "\nโปรดยืนยันการชำระเงิน\nเมื่อรับเงินจากลูกค้าแล้ว", "ยืนยันการชำระเงินรายการ",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE);
                    if (result == JOptionPane.YES_OPTION) {
                        OrderProduct temp;
                        for (int i = 0; i < tblProductList.getRowCount(); i++) {
                            temp = getOrderProductFromTable(i);
                            currentOrderProductID++;
                            orderProductService.add(temp);
                        }
                        if (customer != null) {
                            getUpdatedCustomer();
                            customerService.update(updatedCustomer);
                        }
                        Order order = orderDetail();
                        currentOrderID++;
                        orderServive.add(order);
                        clearCustomerDetail();
                        enableFunction(false);
                        
                    }
                }
            } else if (combPayMethod.getSelectedItem().toString().equals("บัตรเครดิต / เดบิต")) {
                int result = JOptionPane.showConfirmDialog(null, "โปรดยืนยันการชำระเงิน\nเมื่อรับเงินจากลูกค้าแล้ว", "ยืนยันการชำระเงินรายการ",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (result == JOptionPane.YES_OPTION) {
                    OrderProduct temp;
                    for (int i = 0; i < tblProductList.getRowCount(); i++) {
                        temp = getOrderProductFromTable(i);
                        currentOrderProductID++;
                        orderProductService.add(temp);
                    }
                    if (customer != null) {
                        getUpdatedCustomer();
                        customerService.update(updatedCustomer);
                    }
                    Order order = orderDetail();
                    currentOrderID++;
                    orderServive.add(order);
                    clearCustomerDetail();
                    enableFunction(false);
                }
            } else if (combPayMethod.getSelectedItem().toString().equals("พร้อมเพย์")) {
                int result = JOptionPane.showConfirmDialog(null, "โปรดยืนยันการชำระเงิน\nเมื่อรับเงินจากลูกค้าแล้ว ?", "ยืนยันการชำระเงินรายการ",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (result == JOptionPane.YES_OPTION) {
                    OrderProduct temp;
                    for (int i = 0; i < tblProductList.getRowCount(); i++) {
                        temp = getOrderProductFromTable(i);
                        currentOrderProductID++;
                        orderProductService.add(temp);
                    }
                    if (customer != null) {
                        getUpdatedCustomer();
                        customerService.update(updatedCustomer);
                    }
                    Order order = orderDetail();
                    currentOrderID++;
                    orderServive.add(order);
                    clearCustomerDetail();
                    enableFunction(false);
                }
            }
        } else {
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f, "ไม่สามารถชำระรายการได้ เนื่องจากรายการยังว่างอยู่", "ไม่สามารถชำระรายการได้", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnPayActionPerformed

    public void enableFunction(boolean status) {
        btnIncrease.setEnabled(status);
        btnDecrease.setEnabled(status);
        btnRemove.setEnabled(status);
        txtDiscount.setEditable(status);
        txtCusPointUse.setEditable(status);
        txtReceive.setEditable(status);
        btnCancel.setEnabled(status);
        btnPrint.setEnabled(status);
        btnPay.setEnabled(status);
        if (status == false) {
            lblOrder.setText("รายการ");
            model.setRowCount(0);
            txtSubtotal.setText("");
            txtMemberDiscount.setText("");
            txtDiscount.setText("");
            txtChange.setText("");
            txtCusPointRemain.setText("");
            txtTotal.setText("");
            txtReceive.setText("");
            txtTax.setText("");
            txtTotal.setText("");
            txtCusNOP.setText("");
            txtCusPoint.setText("");
            txtCusPointReceive.setText("");
            txtCusPointUse.setText("");
            txtReceive.setText("");
            txtChange.setText("");
            txtTotal1.setText("");
        }
    }

    private void txtReceiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtReceiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReceiveActionPerformed

    private void txtChangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtChangeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtChangeActionPerformed

    private void txtCusPointUseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCusPointUseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCusPointUseActionPerformed

    private void txtDiscountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDiscountKeyTyped
        if (!"".equals(txtDiscount.getText())) {
            double total = Double.parseDouble(txtSubtotal.getText());
            double discount = Double.parseDouble(txtDiscount.getText());
            String newSubtotal = String.format("%.2f", total - discount);
            txtTotal.setText(newSubtotal);
        }
    }//GEN-LAST:event_txtDiscountKeyTyped

    private void txtDiscountPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_txtDiscountPropertyChange

    }//GEN-LAST:event_txtDiscountPropertyChange

    private void txtDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountActionPerformed

    }//GEN-LAST:event_txtDiscountActionPerformed

    private void txtDiscountMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtDiscountMouseExited

    }//GEN-LAST:event_txtDiscountMouseExited

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void btnIncreaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncreaseActionPerformed
        int updateRow = tblProductList.getSelectedRow();
        if (updateRow >= 0) {
            int quantity = (int) tblProductList.getValueAt(updateRow, 4);
            int price = (int) tblProductList.getValueAt(updateRow, 5);
            tblProductList.setValueAt(quantity + 1, updateRow, 4);
            tblProductList.setValueAt((price / quantity) * (quantity + 1), updateRow, 5);
            getItemCost();
        }
    }//GEN-LAST:event_btnIncreaseActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        int removeCol = tblProductList.getSelectedRow();
        if (removeCol >= 0) {
            model.removeRow(removeCol);
        }
        if (tblProductList.getRowCount() == 0) {
            enableFunction(false);
        }
        getItemCost();
        for (int i = 0; i < tblProductList.getRowCount(); i++) {
            tblProductList.setValueAt(i + 1, i, 0);
        }
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnDecreaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDecreaseActionPerformed
        int updateRow = tblProductList.getSelectedRow();
        if (updateRow >= 0) {
            int quantity = (int) tblProductList.getValueAt(updateRow, 4);
            int price = (int) tblProductList.getValueAt(updateRow, 5);
            if (quantity == 1) {
                return;
            }
            tblProductList.setValueAt(quantity - 1, updateRow, 4);
            tblProductList.setValueAt((price / quantity) * (quantity - 1), updateRow, 5);
            getItemCost();
        }
    }//GEN-LAST:event_btnDecreaseActionPerformed

    private void txtTotal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotal1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotal1ActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        if (tblProductList.getRowCount() > 0) {
            MessageFormat header = new MessageFormat("รายการที่ " + currentOrderID);
            MessageFormat footer = new MessageFormat("หน้าที่ {0, number, integer}");

            try {
                tblProductList.print(JTable.PrintMode.NORMAL, header, footer);
            } catch (java.awt.print.PrinterException e) {
                System.out.println("No printer found");
            }
        } else {
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f, "ไม่สามารถพิมพ์รายการได้ เนื่องจากรายการยังว่างอยู่", "ไม่สามารถพิมพ์รายการได้", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnPrintActionPerformed

    private void txtDateTimeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDateTimeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDateTimeActionPerformed

    public Customer getUpdatedCustomer() {
        updatedCustomer.setCusId(customer.getCusId());
        updatedCustomer.setCusName(customer.getCusName());
        updatedCustomer.setCusSurname(customer.getCusSurname());
        updatedCustomer.setCusTel(customer.getCusTel());
        updatedCustomer.setCusPoint(Integer.parseInt(txtCusPointRemain.getText()));
        updatedCustomer.setCusPurchaseCount(updatedCustomer.getCusPurchaseCount() + 1);
        updatedCustomer.setCusDiscount(0);
        return updatedCustomer;
    }

    public void setTxtCustomerPointUse(int value) {
        Runnable doSet = new Runnable() {
            @Override
            public void run() {
                txtCusPointUse.setText(Integer.toString(value));
            }
        };
        SwingUtilities.invokeLater(doSet);
    }

    public void getItemCost() {
        double sum = 0;
        for (int i = 0; i < tblProductList.getRowCount(); i++) {
            sum = sum + Double.parseDouble(tblProductList.getValueAt(i, 5).toString());
        }
        txtSubtotal.setText(Double.toString(sum));
        double cTotal1 = Double.parseDouble(txtSubtotal.getText());
        double cTax = (cTotal1 * 5) / 100;
        String iTaxTotal = String.format("%.2f", cTax);
        if (!iTaxTotal.equals("0.00")) {
            txtTax.setText(iTaxTotal);
        } else {
            txtTax.setText("");
        }

        String iSubtotal = String.format("%.2f", cTotal1);
        if (!iSubtotal.equals("0.00")) {
            txtSubtotal.setText(iSubtotal);
        } else {
            txtSubtotal.setText("");
        }
        double cusDiscount = getCustomerPointUse() / 2;
        String iTotal = String.format("%.2f", cTotal1 + cTax - getDiscountValue() - cusDiscount);
        if (!iTotal.equals("0.00")) {
            txtTotal.setText(iTotal);
            txtTotal1.setText(iTotal);
        } else {
            txtTotal.setText("");
            txtTotal1.setText("");
        }
    }

    public double getDiscountValue() {
        if (txtDiscount.getText().isEmpty()) {
            return 0;
        } else {
            return Double.parseDouble(txtDiscount.getText());
        }
    }

    public int getCustomerPointRemain() {
        if (customer != null) {
            int cusPointReceive;
            if (txtCusPointReceive.getText().isEmpty()) {
                cusPointReceive = 0;
            } else {
                cusPointReceive = Integer.parseInt(txtCusPointReceive.getText());
            }
            int cusPointUse;
            if (txtCusPointUse.getText().isEmpty()) {
                cusPointUse = 0;
            } else {
                cusPointUse = Integer.parseInt(txtCusPointUse.getText());
            }
            return customer.getCusPoint() + cusPointReceive - cusPointUse;
        } else {
            return -1;
        }
    }

    public double getCustomerPointUse() {
        if (txtCusPointUse.getText().isEmpty()) {
            return 0;
        } else {
            return Double.parseDouble(txtCusPointUse.getText());
        }
    }

    public double getMemberDiscountValue() {
        if (txtMemberDiscount.getText().isEmpty()) {
            return 0;
        } else {
            return Double.parseDouble(txtMemberDiscount.getText());
        }
    }

    public void getChange() {
        double sum = Double.parseDouble(txtTotal1.getText());
        double cash = Double.parseDouble(txtReceive.getText());

        double cChange = cash - sum;
        if (cChange < 0) {
            cChange = -1;
        }
        String changeGiven = String.format("%.2f", cChange);
        txtChange.setText(changeGiven);
    }

    public boolean isTableisEmpty() {
        if (model.getRowCount() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public void enableCustomerPointUseTxt() {
        if (model.getRowCount() > 0) {
            txtCusPointUse.setEditable(true);
        }
    }

    public void clearCustomerDetail() {
        customer = null;
        txtCusNOP.setText("");
        txtCusPoint.setText("");
        txtCusPointReceive.setText("");
        txtCusPointRemain.setText("");
        txtCusPointUse.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDecrease;
    private javax.swing.JButton btnIncrease;
    private javax.swing.JButton btnMemberReg;
    private javax.swing.JButton btnPay;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnSearchMember;
    private javax.swing.JComboBox<String> combPayMethod;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblMemberDiscount;
    private javax.swing.JLabel lblNOP;
    private javax.swing.JLabel lblOrder;
    private javax.swing.JLabel lblPoint1;
    private javax.swing.JLabel lblPointReceive1;
    private javax.swing.JLabel lblPointRemain1;
    private javax.swing.JLabel lblPointUse1;
    private javax.swing.JLabel lblPoints;
    private javax.swing.JLabel lblReceive;
    private javax.swing.JLabel lblSubtotal;
    private javax.swing.JLabel lblTax1;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTotal2;
    private javax.swing.JLabel lblTotal3;
    private javax.swing.JPanel pnlOrder;
    private javax.swing.JPanel pnlTotal1;
    private javax.swing.JPanel pnlTotal2;
    private javax.swing.JScrollPane scpProduct;
    private javax.swing.JScrollPane scpProductList;
    private javax.swing.JTable tblProductList;
    private javax.swing.JTextField txtChange;
    private javax.swing.JTextField txtCusNOP;
    private javax.swing.JTextField txtCusPoint;
    private javax.swing.JTextField txtCusPointReceive;
    private javax.swing.JTextField txtCusPointRemain;
    private javax.swing.JTextField txtCusPointUse;
    private javax.swing.JTextField txtDateTime;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JTextField txtMemberDiscount;
    private javax.swing.JTextField txtReceive;
    private javax.swing.JTextField txtSubtotal;
    private javax.swing.JTextField txtTax;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtTotal1;
    // End of variables declaration//GEN-END:variables
}
