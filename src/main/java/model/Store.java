/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bwstx
 */
public class Store {
    private int storeId;
    private int empId;
    private String storeName;
    private String storeAddress;
    private String storeTel;
    private int storeTax;
    private String storeLogo;
    

    public Store(int storeId, int empId, String storeName, String storeAddress, String storeTel, int storeTax, String storeLogo) {
        this.storeId = storeId;
        this.empId = empId;
        this.storeName = storeName;
        this.storeAddress = storeAddress;
        this.storeTel = storeTel;
        this.storeTax = storeTax;
        this.storeLogo = storeLogo;
    }

    public Store(int empId, String storeName, String storeAddress, String storeTel, int storeTax, String storeLogo) {
        this.storeId = -1;
        this.empId = empId;
        this.storeName = storeName;
        this.storeAddress = storeAddress;
        this.storeTel = storeTel;
        this.storeTax = storeTax;
        this.storeLogo = storeLogo;
    }

    public Store() {
        this.storeId = 1;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStoreTel() {
        return storeTel;
    }

    public void setStoreTel(String storeTel) {
        this.storeTel = storeTel;
    }

    public int getStoreTax() {
        return storeTax;
    }

    public void setStoreTax(int storeTax) {
        this.storeTax = storeTax;
    }

    public String getStoreLogo() {
        return storeLogo;
    }

    public void setStoreLogo(String storeLogo) {
        this.storeLogo = storeLogo;
    }

    @Override
    public String toString() {
        return "Store{" + "storeId=" + storeId + ", empId=" + empId + ", storeName=" + storeName + ", storeAddress=" + storeAddress + ", storeTel=" + storeTel + ", storeTax=" + storeTax + ", storeLogo=" + storeLogo + '}';
    }
    
    public static Store fromRS(ResultSet rs) {
        Store store = new Store();
        try {
            store.setStoreId(rs.getInt("store_id"));
            store.setEmpId(rs.getInt("emp_id"));
            store.setStoreName(rs.getString("store_name"));
            store.setStoreAddress(rs.getString("store_address"));
            store.setStoreTel(rs.getString("store_tel"));
            store.setStoreTax(rs.getInt("store_tax"));
            store.setStoreLogo(rs.getString("store_logo"));
        } catch (SQLException ex) {
            Logger.getLogger(Store.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return store;
    }  
}
