/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Service.ProductService;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class Product {

    private int prdID;
    private String prdName;
    private String prdSize;
    private String prdType;
    private String prdSweetLevel;
    private double prdPrice;

    public Product(int prdID, String prdName, String prdSize, String prdType, String prdSweetLevel, double prdPrice) {
        this.prdID = prdID;
        this.prdName = prdName;
        this.prdSize = prdSize;
        this.prdType = prdType;
        this.prdSweetLevel = prdSweetLevel;
        this.prdPrice = prdPrice;
    }

    public Product(String prdName, String prdSize, String prdType, String prdSweetLevel, double prdPrice) {
        this.prdID = -1;
        this.prdName = prdName;
        this.prdSize = prdSize;
        this.prdType = prdType;
        this.prdSweetLevel = prdSweetLevel;
        this.prdPrice = prdPrice;
    }

    public Product() {
        this.prdID = -1;
        this.prdName = "";
        this.prdSize = "";
        this.prdType = "";
        this.prdSweetLevel = "";
        this.prdPrice = 0;
    }

    public int getPrdID() {
        return prdID;
    }

    public void setPrdID(int prdID) {
        this.prdID = prdID;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getPrdSize() {
        return prdSize;
    }

    public void setPrdSize(String prdSize) {
        this.prdSize = prdSize;
    }

    public String getPrdType() {
        return prdType;
    }

    public void setPrdType(String prdType) {
        this.prdType = prdType;
    }

    public String getPrdSweetLevel() {
        return prdSweetLevel;
    }

    public void setPrdSweetLevel(String prdSweetLevel) {
        this.prdSweetLevel = prdSweetLevel;
    }

    public double getPrdPrice() {
        return prdPrice;
    }

    @Override
    public String toString() {
        return "Product{" + "prdID=" + prdID + ", prdName=" + prdName + ", prdSize=" + prdSize + ", prdType=" + prdType + ", prdSweetLevel=" + prdSweetLevel + ", prdPrice=" + prdPrice + '}';
    }

    public void setPrdPrice(double prdPrice) {
        this.prdPrice = prdPrice;
    }

    public static Product fromRS(ResultSet rs) {
        Product prd = new Product();
        try {
            prd.setPrdID(rs.getInt("product_id"));
            prd.setPrdName(rs.getString("product_name"));
            prd.setPrdSize(rs.getString("product_size"));
            prd.setPrdType(rs.getString("product_type"));
            prd.setPrdSweetLevel(rs.getString("product_level"));
            prd.setPrdPrice(rs.getDouble("product_price"));
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return prd;
    }
    public static List<Product> getProductList() {
        ProductService prdService = new ProductService();
        List<Product> list = prdService.getProducts();
        return list;
    }
}
