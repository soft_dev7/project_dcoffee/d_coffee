/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class Employee {

    private int empId;
    private String empName;
    private String empSurname;
    private String empTel;
    private String empUser;
    private String empPassword;
    private String empPosition;
    private String empGender;
    private String empDob;
    private String empNat;
    private String empReligion;
    private String empStatus;
    private String empAdress;

    public Employee(int empId, String empName, String empSurname, String empTel, String empUser, String empPassword, String empPosition,
            String empGender, String empDob, String empNat, String empReligion, String empStatus, String empAdress) {
        this.empId = empId;
        this.empName = empName;
        this.empSurname = empSurname;
        this.empTel = empTel;
        this.empUser = empUser;
        this.empPassword = empPassword;
        this.empPosition = empPosition;
        this.empGender = empGender;
        this.empDob = empDob;
        this.empNat = empNat;
        this.empReligion = empReligion;
        this.empStatus = empStatus;
        this.empAdress = empAdress;
    }

    public Employee(String empName, String empSurname, String empTel, String empUser, String empPassword, String empPosition,
            String empGender, String empDob, String empNat, String empReligion, String empStatus, String empAdress) {
        this.empId = -1;
        this.empName = empName;
        this.empSurname = empSurname;
        this.empTel = empTel;
        this.empUser = empUser;
        this.empPassword = empPassword;
        this.empPosition = empPosition;
        this.empGender = empGender;
        this.empDob = empDob;
        this.empNat = empNat;
        this.empReligion = empReligion;
        this.empStatus = empStatus;
        this.empAdress = empAdress;
    }

    public Employee() {
        this.empId = -1;
        this.empName = "";
        this.empSurname = "";
        this.empTel = "";
        this.empUser = "";
        this.empPassword = "";
        this.empPosition = "";
        this.empGender = "";
        this.empDob = "";
        this.empNat = "";
        this.empPosition = "";
        this.empStatus = "";
        this.empAdress = "";
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpSurname() {
        return empSurname;
    }

    public void setEmpSurname(String empSurname) {
        this.empSurname = empSurname;
    }

    public String getEmpTel() {
        return empTel;
    }

    public void setEmpTel(String empTel) {
        this.empTel = empTel;
    }

    public String getEmpUser() {
        return empUser;
    }

    public void setEmpUser(String empUser) {
        this.empUser = empUser;
    }

    public String getEmpPassword() {
        return empPassword;
    }

    public void setEmpPassword(String empPassword) {
        this.empPassword = empPassword;
    }

    public String getEmpPosition() {
        return empPosition;
    }

    public void setEmpPosition(String empPosition) {
        this.empPosition = empPosition;
    }

    public String getEmpGender() {
        return empGender;
    }

    public void setEmpGender(String empGender) {
        this.empGender = empGender;
    }

    public String getEmpDob() {
        return empDob;
    }

    public void setEmpDob(String empDob) {
        this.empDob = empDob;
    }

    public String getEmpNat() {
        return empNat;
    }

    public void setEmpNat(String empNat) {
        this.empNat = empNat;
    }

    public String getEmpReligion() {
        return empReligion;
    }

    public void setEmpReligion(String empReligion) {
        this.empReligion = empReligion;
    }

    public String getEmpStatus() {
        return empStatus;
    }

    public void setEmpStatus(String empStatus) {
        this.empStatus = empStatus;
    }

    public String getEmpAdress() {
        return empAdress;
    }

    public void setEmpAdress(String empAdress) {
        this.empAdress = empAdress;
    }

    @Override
    public String toString() {
        return "Employee{" + "empId=" + empId + ", empName=" + empName + ", empSurname=" + empSurname + ", empTel=" + empTel + ", empUser=" + empUser + ", empPassword=" + empPassword + ", empPosition=" + empPosition + ", empGender=" + empGender + ", empDob=" + empDob + ", empNat=" + empNat + ", empReligion=" + empReligion + ", empStatus=" + empStatus + ", empAdress=" + empAdress + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setEmpId(rs.getInt("emp_id"));
            employee.setEmpName(rs.getString("emp_name"));
            employee.setEmpSurname(rs.getString("emp_surname"));
            employee.setEmpTel(rs.getString("emp_tel"));
            employee.setEmpUser(rs.getString("emp_user"));
            employee.setEmpPassword(rs.getString("emp_password"));
            employee.setEmpPosition(rs.getString("emp_position"));
            employee.setEmpGender(rs.getString("emp_gender"));
            employee.setEmpDob(rs.getString("emp_dob"));
            employee.setEmpNat(rs.getString("emp_nat"));
            employee.setEmpReligion(rs.getString("emp_religion"));
            employee.setEmpStatus(rs.getString("emp_status"));
            employee.setEmpAdress(rs.getString("emp_address"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }

}
