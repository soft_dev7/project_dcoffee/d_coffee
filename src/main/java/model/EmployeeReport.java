/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class EmployeeReport {

    @Override
    public String toString() {
        return "EmployeeReport{" + "empReportId=" + empReportId + ", empId=" + empId + ", tcReportId=" + tcReportId + ", totalSalary=" + totalSalary + ", totalHours=" + totalHours + ", date=" + date + '}';
    }

    private int empReportId;
    private int empId;
    private int tcReportId;
    private int totalSalary;
    private int totalHours;
    private String date;

    public EmployeeReport(int empId, int tcReportId, int totalSalary, int totalHours, String date) {
        this.empReportId = -1;
        this.empId = empId;
        this.tcReportId = tcReportId;
        this.totalSalary = totalSalary;
        this.totalHours = totalHours;
        this.date = date;
    }

    public EmployeeReport(int empReportId, int empId, int tcReportId, int totalSalary, int totalHours, String date) {
        this.empReportId = empReportId;
        this.empId = empId;
        this.tcReportId = tcReportId;
        this.totalSalary = totalSalary;
        this.totalHours = totalHours;
        this.date = date;
    }

    public EmployeeReport() {
        this.empReportId = -1;
        this.empId = 0;
        this.tcReportId = 0;
        this.totalSalary = 0;
        this.totalHours = 0;
        this.date = "";
    }

    public int getEmpReportId() {
        return empReportId;
    }

    public void setEmpReportId(int empReportId) {
        this.empReportId = empReportId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getTcReportId() {
        return tcReportId;
    }

    public void setTcReportId(int tcReportId) {
        this.tcReportId = tcReportId;
    }

    public int getTotalSalary() {
        return totalSalary;
    }

    public void setTotalSalary(int totalSalary) {
        this.totalSalary = totalSalary;
    }

    public int getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(int totalHours) {
        this.totalHours = totalHours;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public static EmployeeReport fromRS(ResultSet rs) {
        EmployeeReport employeeReport = new EmployeeReport();
        try {
            employeeReport.setEmpReportId(rs.getInt("empReport_id"));
            employeeReport.setEmpId(rs.getInt("emp_id"));
            employeeReport.setTcReportId(rs.getInt("tc_report_id"));
            employeeReport.setTotalSalary(rs.getInt("total_salary"));
            employeeReport.setTotalHours(rs.getInt("total_hours"));
            employeeReport.setDate(rs.getString("empReport_date"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employeeReport;
    }
}
