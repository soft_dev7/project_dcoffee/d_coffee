/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class Material {

    private int mtrID;
    private String mtrName;
    private int mtrMin;
    private int mtrOnHand;
    private String mtrUnit;
    private double mtrPrice;

    public Material(String mtrName, int mtrMin, int mtrOnHand, String mtrUnit, Double mtrPrice) {
        this.mtrID = -1;
        this.mtrName = mtrName;
        this.mtrMin = mtrMin;
        this.mtrOnHand = mtrOnHand;
        this.mtrUnit = mtrUnit;
        this.mtrPrice = mtrPrice;
    }

    public Material() {
        this.mtrID = -1;
        this.mtrName = "";
        this.mtrMin = 0;
        this.mtrOnHand = 0;
        this.mtrUnit = "";
        this.mtrPrice = 0;
    }

    public Material(int mtrID, String mtrName, int mtrMin, int mtrOnHand, String mtrUnit) {
        this.mtrID = mtrID;
        this.mtrName = mtrName;
        this.mtrMin = mtrMin;
        this.mtrOnHand = mtrOnHand;
        this.mtrUnit = mtrUnit;
        this.mtrPrice = mtrPrice;
    }

    public int getMtrID() {
        return mtrID;
    }

    public void setMtrID(int mtrID) {
        this.mtrID = mtrID;
    }

    public String getMtrName() {
        return mtrName;
    }

    public void setMtrName(String mtrName) {
        this.mtrName = mtrName;
    }

    public int getMtrMin() {
        return mtrMin;
    }

    public void setMtrMin(int mtrMin) {
        this.mtrMin = mtrMin;
    }

    public int getMtrOnHand() {
        return mtrOnHand;
    }

    public void setMtrOnHand(int mtrOnHand) {
        this.mtrOnHand = mtrOnHand;
    }

    public String getMtrUnit() {
        return mtrUnit;
    }

    public void setMtrUnit(String mtrUnit) {
        this.mtrUnit = mtrUnit;
    }
    
    public Double getMtrPrice() {
        return mtrPrice;
    }

    public void setMtrPrice(Double mtrPrice) {
        this.mtrPrice = mtrPrice;
    }

    @Override
    public String toString() {
        return "Materia ID = " + mtrID + ", Name = " + mtrName + ", Minimum = " + mtrMin + ", On hand = " + mtrOnHand + ", Unit = " + mtrUnit + ", mtrPrice = "+mtrPrice;
    }

    public static Material fromRS(ResultSet rs) {
        Material mtr = new Material();
        try {
            mtr.setMtrID(rs.getInt("mtr_id"));
            mtr.setMtrName(rs.getString("mtr_name"));
            mtr.setMtrMin(rs.getInt("mtr_min"));
            mtr.setMtrOnHand(rs.getInt("mrt_onhand"));
            mtr.setMtrUnit(rs.getString("mtr_unit"));
            mtr.setMtrPrice(rs.getDouble("mtr_price"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return mtr;
    }
}
