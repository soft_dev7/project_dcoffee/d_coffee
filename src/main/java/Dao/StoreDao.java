/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import DatabaseHelper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Store;

/**
 *
 * @author bwstx
 */
public class StoreDao implements Dao<Store> {

    @Override
    public Store get(int id) {
        Store item = null;
        String sql = "SELECT * FROM store WHERE store_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Store.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public Store getLast() {
        Store item = null;
        String sql = "SELECT * FROM store ORDER BY store_id DESC LIMIT 1";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Store.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public List<Store> getAll() {
        ArrayList<Store> list = new ArrayList();
        String sql = "SELECT * FROM store";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Store item = Store.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Store> getAll(String where, String order) {
        ArrayList<Store> list = new ArrayList();
        String sql = "SELECT * FROM store where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Store item = Store.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Store> getAll(String order) {
        ArrayList<Store> list = new ArrayList();
        String sql = "SELECT * FROM store ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Store item = Store.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Store save(Store obj) {

        String sql = "INSERT INTO store (emp_id, store_name, store_address, store_tel, store_tax, store_logo)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpId());
            stmt.setString(2, obj.getStoreName());
            stmt.setString(3, obj.getStoreAddress());
            stmt.setString(4, obj.getStoreTel());
            stmt.setInt(5, obj.getStoreTax());
            stmt.setString(6, obj.getStoreLogo());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setStoreId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Store update(Store obj) {
        String sql = "UPDATE store"
                + " SET emp_id = ?, store_name = ?, store_address = ?, store_tel = ?, store_tax = ?, store_logo = ? "
                + " WHERE store_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            System.out.println(obj);
            stmt.setInt(1, obj.getEmpId());
            stmt.setString(2, obj.getStoreName());
            stmt.setString(3, obj.getStoreAddress());
            stmt.setString(4, obj.getStoreTel());
            stmt.setInt(5, obj.getStoreTax());
            stmt.setString(6, obj.getStoreLogo());
            stmt.setInt(7, obj.getStoreId());

            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Store obj) {
        String sql = "DELETE FROM store WHERE store_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getStoreId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
