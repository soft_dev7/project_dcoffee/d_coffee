/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import DatabaseHelper.DatabaseHelper;
import TimeClockReport.TimeClock;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Customer;

/**
 *
 * @author bwstx
 */
public class TimeClockDao implements Dao<TimeClock> {
    public TimeClock get(int id) {
        TimeClock item = null;
        String sql = "SELECT * FROM timeclock_report WHERE tc_report_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = TimeClock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }
    
        public TimeClock getLast() {
        TimeClock item = null;
        String sql = "SELECT * FROM timeclock_report ORDER BY tc_report_id DESC LIMIT 1";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = TimeClock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public List<TimeClock> getAll() {
        ArrayList<TimeClock> list = new ArrayList();
        String sql = "SELECT * FROM timeclock_report";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                TimeClock item = TimeClock.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<TimeClock> getAll(String where, String order) {
        ArrayList<TimeClock> list = new ArrayList();
        String sql = "SELECT * FROM timeclock_report where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                TimeClock item = TimeClock.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<TimeClock> getAll(String order) {
        ArrayList<TimeClock> list = new ArrayList();
        String sql = "SELECT * FROM timeclock_report ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                TimeClock item = TimeClock.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public TimeClock save(TimeClock obj) {

        String sql = "INSERT INTO timeclock_report (tc_report_id, store_id, emp_id, tc_date, tc_in, tc_out, tc_hours)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getTcReportId());
            stmt.setInt(2, obj.getStoreId());
            stmt.setInt(3, obj.getEmpId());
            stmt.setString(4, obj.getTcDate());
            stmt.setString(5, obj.getTcIn());
            stmt.setString(6, obj.getTcOut());
            stmt.setInt(7, obj.getTcHours());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setTcReportId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public TimeClock update(TimeClock obj) {
        String sql = "UPDATE timeclock_report"
                + " SET store_id = ?, emp_id = ?, tc_date = ?, tc_in = ?, tc_out = ?, tc_hours = ? "
                + " WHERE tc_report_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getStoreId());
            stmt.setInt(2, obj.getEmpId());
            stmt.setString(3, obj.getTcDate());
            stmt.setString(4, obj.getTcIn());
            stmt.setString(5, obj.getTcOut());
            stmt.setInt(6, obj.getTcHours());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(TimeClock obj) {
        String sql = "DELETE FROM timeclock_report WHERE tc_report_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getTcReportId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
}
