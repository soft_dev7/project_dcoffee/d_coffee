/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import DatabaseHelper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Employee;

/**
 *
 * @author Acer
 */
public class EmployeeDao implements Dao<Employee> {

    @Override
    public Employee get(int id) {
        Employee item = null;
        String sql = "SELECT * FROM employee WHERE emp_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public Employee getLast() {
        Employee item = null;
        String sql = "SELECT * FROM employee ORDER BY emp_id DESC LIMIT 1";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public Employee getByLoginDetail(String userName, String password, String position) {
        Employee item = null;
        String sql = "SELECT * FROM employee WHERE emp_user=\"" + userName + "\" AND emp_password=\"" + password + "\" AND emp_position=\"" + position + "\"";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                item = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public Employee getID(int id) {
        Employee item = null;
        String sql = "SELECT * FROM employee WHERE emp_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Employee> getAll() {

        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM employee";
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee item = Employee.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Employee> getAll(String where, String order) {
        ArrayList<Employee> list = new ArrayList<Employee>();
        String sql = "SELECT * FROM employee where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee item = Employee.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Employee> getAll(String order) {

        ArrayList<Employee> list = new ArrayList<Employee>();

        String sql = "SELECT * FROM employee  ORDER BY" + order;
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee item = Employee.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        DatabaseHelper.close();
        return list;
    }

    public List<Employee> getByPosition(String role) {

        ArrayList<Employee> list = new ArrayList<Employee>();

        String sql = "SELECT * FROM employee WHERE emp_position=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, role);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Employee item = Employee.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Employee save(Employee obj) {
        String sql = "INSERT INTO employee (emp_id, emp_name, emp_surname, emp_tel, emp_user, emp_password, emp_position, emp_gender, emp_dob, emp_nat, emp_religion, emp_status, emp_address)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpId());
            stmt.setString(2, obj.getEmpName());
            stmt.setString(3, obj.getEmpSurname());
            stmt.setString(4, obj.getEmpTel());
            stmt.setString(5, obj.getEmpUser());
            stmt.setString(6, obj.getEmpPassword());
            stmt.setString(7, obj.getEmpPosition());
            stmt.setString(8, obj.getEmpGender());
            stmt.setString(9, obj.getEmpDob());
            stmt.setString(10, obj.getEmpNat());
            stmt.setString(11, obj.getEmpReligion());
            stmt.setString(12, obj.getEmpStatus());
            stmt.setString(13, obj.getEmpAdress());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setEmpId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Employee update(Employee obj) {
        String sql = "UPDATE employee"
                + " SET emp_name = ?, emp_surname = ?, emp_tel = ?, emp_user = ?, emp_password = ?, emp_position = ?, emp_gender = ?, emp_dob = ?, emp_nat = ?, emp_religion = ? , emp_status = ?, emp_address = ?"
                + " WHERE emp_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getEmpName());
            stmt.setString(2, obj.getEmpSurname());
            stmt.setString(3, obj.getEmpTel());
            stmt.setString(4, obj.getEmpUser());
            stmt.setString(5, obj.getEmpPassword());
            stmt.setString(6, obj.getEmpPosition());
            stmt.setString(7, obj.getEmpGender());
            stmt.setString(8, obj.getEmpDob());
            stmt.setString(9, obj.getEmpNat());
            stmt.setString(10, obj.getEmpReligion());
            stmt.setString(11, obj.getEmpStatus());
            stmt.setString(12, obj.getEmpAdress());
            stmt.setInt(13, obj.getEmpId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Employee obj) {
        String sql = "DELETE FROM employee WHERE emp_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
