/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import DatabaseHelper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Employee;
import model.EmployeeReport;

/**
 *
 * @author Acer
 */
public class EmployeeReportDao implements Dao<EmployeeReport> {

    @Override
    public EmployeeReport get(int id) {
        EmployeeReport item = null;
        String sql = "SELECT * FROM emp_report WHERE empReport_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = EmployeeReport.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public EmployeeReport getLast() {
        EmployeeReport item = null;
        String sql = "SELECT * FROM emp_report ORDER BY empReport_id DESC LIMIT 1";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = EmployeeReport.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<EmployeeReport> getAll() {

        ArrayList<EmployeeReport> list = new ArrayList();
        String sql = "SELECT * FROM emp_report";
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                EmployeeReport item = EmployeeReport.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<EmployeeReport> getAll(String where, String order) {
        ArrayList<EmployeeReport> list = new ArrayList<EmployeeReport>();
        String sql = "SELECT * FROM emp_report where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                EmployeeReport item = EmployeeReport.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<EmployeeReport> getAll(String order) {

        ArrayList<EmployeeReport> list = new ArrayList<EmployeeReport>();

        String sql = "SELECT * FROM emp_report  ORDER BY" + order;
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                EmployeeReport item = EmployeeReport.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        DatabaseHelper.close();
        return list;
    }

    @Override
    public EmployeeReport save(EmployeeReport obj) {
        return null;
    }

    @Override
    public EmployeeReport update(EmployeeReport obj) {
        return null;
    }

    @Override
    public int delete(EmployeeReport obj) {
        return -1;
    }

    public Employee save(Employee editedEmpRpt) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
