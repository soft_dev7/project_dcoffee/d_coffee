/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import Dao.StoreDao;
import java.util.List;
import model.Store;

/**
 *
 * @author bwstx
 */
public class StoreService {
    public List<Store> getStore() {
        StoreDao storeDao = new StoreDao();
        return storeDao.getAll(" store_id asc");
    }
    
    public Store getLast(){
        StoreDao storeDao = new StoreDao();
        return storeDao.getLast();
    }
    
    public Store getStoreByid(int id){
        StoreDao storeDao = new StoreDao();
        return storeDao.get(id);
    }

    public Store add(Store editStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.save(editStore);
    }

    public Store update(Store editStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.update(editStore);
    }

    public int delete(Store editStore) {
        StoreDao mtrDao = new StoreDao();
        return mtrDao.delete(editStore);
    }
}
