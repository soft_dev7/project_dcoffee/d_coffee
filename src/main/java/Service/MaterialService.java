/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import Dao.MaterialDao;
import java.util.List;
import model.Material;

/**
 *
 * @author Acer
 */
public class MaterialService {

    public List<Material> getMaterials() {
        MaterialDao mtrDao = new MaterialDao();
        return mtrDao.getAll(" mtr_id asc");
    }

    public Material add(Material editedMtr) {
        MaterialDao mtrDao = new MaterialDao();
        return mtrDao.save(editedMtr);
    }

    public Material update(Material editedMtr) {
        MaterialDao mtrDao = new MaterialDao();
        return mtrDao.update(editedMtr);
    }

    public int delete(Material editedMtr) {
        MaterialDao mtrDao = new MaterialDao();
        return mtrDao.delete(editedMtr);
    }

}
