/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import Dao.EmployeeDao;
import java.util.List;
import model.Employee;

/**
 *
 * @author Acer
 */
public class EmployeeService {

    public List<Employee> getEmployees() {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getAll(" emp_id asc");
    }

    public List<Employee> getEmployeeByPosition(String position) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getByPosition(position);
    }

    public Employee getLast() {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getLast();
    }

    public Employee getEmployeeByLoginDetail(String userName, String password, String position) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getByLoginDetail(userName, password, position);
    }

    public Employee add(Employee editedEmp) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.save(editedEmp);
    }

    public Employee update(Employee editedEmp) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.update(editedEmp);
    }

    public int delete(Employee editedEmp) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.delete(editedEmp);
    }

    public Employee getEmployeeID(int id) {
        EmployeeDao cusDao = new EmployeeDao();
        return cusDao.get(id);
    }
}
