/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import Dao.EmployeeReportDao;
import java.util.List;
import model.Employee;
import model.EmployeeReport;

/**
 *
 * @author Acer
 */
public class EmployeeReportService {

    public List<EmployeeReport> getEmployeeReports() {
        EmployeeReportDao empRptDao = new EmployeeReportDao();
        return empRptDao.getAll(" empReport_id asc");
    }

    public Employee add(Employee editedEmpRpt) {
        EmployeeReportDao empRptDao = new EmployeeReportDao();
        return empRptDao.save(editedEmpRpt);
    }

    public EmployeeReport getLast() {
        EmployeeReportDao empRptDao = new EmployeeReportDao();
        return empRptDao.getLast();
    }
}
