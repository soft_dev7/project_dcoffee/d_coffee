/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import Dao.TimeClockDao;
import TimeClockReport.TimeClock;
import java.util.List;

/**
 *
 * @author bwstx
 */
public class TimeClockService {

    public List<TimeClock> getTimeClock() {
        TimeClockDao tcDao = new TimeClockDao();
        return tcDao.getAll(" tc_report_id asc");
    }

    public TimeClock getLast() {
        TimeClockDao tcDao = new TimeClockDao();
        return tcDao.getLast();
    }

    public TimeClock add(TimeClock edittimeClock) {
        TimeClockDao tcDao = new TimeClockDao();
        return tcDao.save(edittimeClock);
    }

    public TimeClock update(TimeClock edittimeClock) {
        TimeClockDao tcDao = new TimeClockDao();
        return tcDao.update(edittimeClock);
    }

    public int delete(TimeClock edittimeClock) {
        TimeClockDao tcDao = new TimeClockDao();
        return tcDao.delete(edittimeClock);
    }
}
