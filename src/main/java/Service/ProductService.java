/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import Dao.ProductDao;
import model.Product;
import java.util.List;

/**
 *
 * @author Acer
 */
public class ProductService {

    public List<Product> getProducts() {
        ProductDao prdDao = new ProductDao();
        return prdDao.getAll(" product_id asc");
    }

    public Product add(Product editedPrd) {
        ProductDao prdDao = new ProductDao();
        return prdDao.save(editedPrd);
    }

    public Product update(Product editedPrd) {
        ProductDao prdDao = new ProductDao();
        return prdDao.update(editedPrd);
    }

    public int delete(Product editedPrd) {
        ProductDao prdDao = new ProductDao();
        return prdDao.delete(editedPrd);
    }

}
