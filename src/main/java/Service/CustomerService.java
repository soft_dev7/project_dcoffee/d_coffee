/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import Dao.CustomerDao;
import java.util.List;
import model.Customer;

/**
 *
 * @author Acer
 */
public class CustomerService {

    public List<Customer> getCustomers() {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.getAll(" cus_id asc");
    }

    public Customer add(Customer editedCus) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.save(editedCus);
    }

    public Customer update(Customer editedCus) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.update(editedCus);
    }

    public int delete(Customer editedCus) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.delete(editedCus);
    }
    
    public Customer getCustomersTel(String tel) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.getCustomersTel(tel);
    }
     public Customer getLast() {
        CustomerDao cusrDao = new CustomerDao();
        return cusrDao.getLast();
    }
}
