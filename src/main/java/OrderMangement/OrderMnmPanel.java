/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package OrderMangement;

import Dao.OrderDao;
import Service.OrderService;
import Service.OrderProductService;
import java.awt.Font;
import java.awt.print.PrinterException;
import java.text.MessageFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import model.Order;
import model.OrderProduct;

/**
 *
 * @author NonWises
 */
public class OrderMnmPanel extends javax.swing.JPanel {

    private OrderService orderService;
    private OrderProductService orderPrdService;
    private List<OrderProduct> ordPrdList;
    private List<Order> orderList;
    private OrderDao ordDao;
    private AbstractTableModel ordPrdTblModel;

    /**
     * Creates new form OrderMangement
     */
    public OrderMnmPanel() {
        initComponents();
        ordDao = new OrderDao();
        orderService = new OrderService();
        orderPrdService = new OrderProductService();
        //ordPrdList = orderPrdService.getOrderProductsByOrdID();
        orderList = orderService.getOrders();
        tblOrder.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblOrderPrd.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        UIManager.put("OptionPane.messageFont", new Font("Tahoma", Font.BOLD, 14));

        tblOrder.setModel(new AbstractTableModel() {
            String[] columnNames = {"รหัสใบเสร็จ", "ประเภทสมาชิก", "รหัสพนักงาน", "ราคารวม(ใบเสร็จ)", "ส่วนลดรวม", "เงินที่ได้รับ(บาท)", "เงินทอน(บาท)",
                "วันที่ซื้อ"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return orderList.size();
            }

            @Override
            public int getColumnCount() {
                return 8;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Order order = orderList.get(rowIndex);
                //System.out.println(ordPrd);

                switch (columnIndex) {
                    case 0:
                        // System.out.println(ordPrd.getOrderId());
                        return order.getOrderId();
                    case 1:
                        if (order.getCusId() < 0) {
                            return "ทั่วไป";
                        } else {
                            return "สมาชิก";
                        }
                    case 2:
                        return order.getEmpId();
                    case 3:
                        return order.getOrderTotal();
                    case 4:
                        if (order.getOrderDiscount() < 1) {
                            return 0.0;
                        } else {
                            return order.getOrderDiscount();
                        }

                    case 5:
                        return order.getOrderCash();
                    case 6:
                        if (order.getOrderChange() < 1) {
                            return 0.0;
                        } else {
                            return order.getOrderChange();
                        }
                    case 7:
                        return order.getOrderDateTime();

                    default:
                        return "Unknow";
                }

            }
        });

        ordPrdTblModel = new AbstractTableModel() {

            String[] columnNames = {"รหัสสินค้า", "ชื่อสินค้า", "ขนาดสินค้า", "ประเภทสินค้า", "ระดับความหวาน", "จำนวน", "ราคา(บาท)", "ราคารวม(บาท)"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return ordPrdList.size();
            }

            @Override
            public int getColumnCount() {
                return 8;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                OrderProduct ordPrd = ordPrdList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return ordPrd.getOrderProductId();
                    case 1:
                        return ordPrd.getProductName();
                    case 2:
                        return ordPrd.getProductSize();
                    case 3:
                        return ordPrd.getProductType();
                    case 4:
                        return ordPrd.getProductSweetness();
                    case 5:
                        return ordPrd.getProductAmount();
                    case 6:
                        return ordPrd.getProductPrice();
                    case 7:
                        return ordPrd.getTotal();
                    default:
                        return "Unknown";
                }
            }
        };
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtOrderID = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOrder = new javax.swing.JTable();
        btnPrint = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblOrderPrd = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        txtOrderID.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtOrderID.setToolTipText("");
        txtOrderID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOrderIDActionPerformed(evt);
            }
        });

        btnSearch.setBackground(new java.awt.Color(84, 70, 63));
        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnSearch.setText("ค้นหา");
        btnSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(84, 70, 63));
        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnDelete.setForeground(new java.awt.Color(255, 255, 255));
        btnDelete.setText("ลบข้อมูล");
        btnDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        tblOrder.setBackground(new java.awt.Color(211, 197, 186));
        tblOrder.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblOrder.setToolTipText("");
        tblOrder.setRowHeight(40);
        tblOrder.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblOrderMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblOrder);

        btnPrint.setBackground(new java.awt.Color(84, 70, 63));
        btnPrint.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnPrint.setForeground(new java.awt.Color(255, 255, 255));
        btnPrint.setText("พิมพ์");
        btnPrint.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        tblOrderPrd.setBackground(new java.awt.Color(211, 197, 186));
        tblOrderPrd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblOrderPrd.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "รหัสสินค้า", "ชื่อสินค้า", "ขนาดสินค้า", "ประเภทสินค้า", "ระดับความหวาน", "จำนวน", "ราคา(บาท)", "ราคารวม(บาท)"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblOrderPrd.setToolTipText("");
        tblOrderPrd.setRowHeight(40);
        jScrollPane2.setViewportView(tblOrderPrd);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("รายการใบเสร็จ");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel2.setText("รายการสินค้า");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtOrderID, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDelete))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(0, 817, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOrderID, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtOrderIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOrderIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOrderIDActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        if (txtOrderID.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "ข้อมูลไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง");
            refreshOrderTable();
            resetOrdPrdTable();
        } else {
            int id = Integer.parseInt(txtOrderID.getText());
            orderList = orderService.getOrdersByID(id);
            refreshOrderTable(orderList);
        }
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedtblOrderIndex = tblOrder.getSelectedRow();
        int selectedtblOrderPrdIndex = tblOrderPrd.getSelectedRow();
        if (selectedtblOrderIndex >= 0 && selectedtblOrderPrdIndex < 0) {
            Order order = orderList.get(selectedtblOrderIndex);
            int deleteConfirm = JOptionPane.showConfirmDialog(this, "Delete all selected order row ?", "Confirm deletion", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (deleteConfirm == 0) {
                orderService.delete(order);
                orderPrdService.deleteByOrderID(order.getOrderId());
                refreshOrderTable();
                refreshOrdPrdTable(order.getOrderId());
            }
        } else if (selectedtblOrderPrdIndex >= 0) {
            OrderProduct ordPrd = ordPrdList.get(selectedtblOrderPrdIndex);
            int deleteConfirm = JOptionPane.showConfirmDialog(this, "Delete all selected order row ?", "Confirm deletion", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (deleteConfirm == 0) {
                orderPrdService.delete(ordPrd);
                Order ord = orderList.get(selectedtblOrderIndex);
                refreshOrdPrdTable(ord.getOrderId());
            }
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        if (tblOrder.getRowCount() > 0) {
            MessageFormat header = new MessageFormat("รายการข้อมูลคำสั่งซื้อ ");
            MessageFormat footer = new MessageFormat("หน้าที่ {0, number, integer}");
            try {
                tblOrder.print(JTable.PrintMode.NORMAL, header, footer);
            } catch (PrinterException ex) {
                Logger.getLogger(OrderMnmPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {

        }
    }//GEN-LAST:event_btnPrintActionPerformed

    private void tblOrderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOrderMouseClicked

        int selectedIndex = tblOrder.getSelectedRow();

        if (selectedIndex >= 0) {
            Order ord = orderList.get(selectedIndex);

            ordPrdList = orderPrdService.getOrderProductByOrdID(ord.getOrderId());
//            tblOrderPrd.setModel(new AbstractTableModel() {
//
//                String[] columnNames = {"รหัสสินค้า", "ชื่อสินค้า", "ขนาดสินค้า", "ประเภทสินค้า", "ระดับความหวาน", "จำนวน", "ราคา(บาท)", "ราคารวม(บาท)"};
//
//                @Override
//                public String getColumnName(int column) {
//                    return columnNames[column];
//                }
//
//                @Override
//                public int getRowCount() {
//                    return ordPrdList.size();
//                }
//
//                @Override
//                public int getColumnCount() {
//                    return 8;
//                }
//
//                @Override
//                public Object getValueAt(int rowIndex, int columnIndex) {
//                    OrderProduct ordPrd = ordPrdList.get(rowIndex);
//                    switch (columnIndex) {
//                        case 0:
//                            return ordPrd.getOrderProductId();
//                        case 1:
//                            return ordPrd.getProductName();
//                        case 2:
//                            return ordPrd.getProductSize();
//                        case 3:
//                            return ordPrd.getProductType();
//                        case 4:
//                            return ordPrd.getProductSweetness();
//                        case 5:
//                            return ordPrd.getProductAmount();
//                        case 6:
//                            return ordPrd.getProductPrice();
//                        case 7:
//                            return ordPrd.getTotal();
//                        default:
//                            return "Unknown";
//                    }
//                }
//            });
            tblOrderPrd.setModel(ordPrdTblModel);
            refreshOrdPrdTable(ord.getOrderId());
        }
    }//GEN-LAST:event_tblOrderMouseClicked

    public void refreshOrdPrdTable(int id) {
        ordPrdList = orderPrdService.getOrderProductByOrdID(id);
        tblOrderPrd.revalidate();
        tblOrderPrd.repaint();
    }
    public void resetOrdPrdTable() {
        tblOrderPrd.setModel(new DefaultTableModel(null, new String[]{"รหัสสินค้า","ชื่อสินค้า","ขนาดสินค้า","ประเภทสินค้า","ระดับความหวาน","จำนวน","ราคา(บาท)","ราคารวม(บาท)"}));
        tblOrderPrd.revalidate();
        tblOrderPrd.repaint();
    }

    public void refreshOrderTable() {
        orderList = orderService.getOrders();
        tblOrder.revalidate();
        tblOrder.repaint();
    }
    public void refreshOrderTable(List<Order> list) {
        orderList = list;
        tblOrder.revalidate();
        tblOrder.repaint();
    }




    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblOrder;
    private javax.swing.JTable tblOrderPrd;
    private javax.swing.JTextField txtOrderID;
    // End of variables declaration//GEN-END:variables
}
